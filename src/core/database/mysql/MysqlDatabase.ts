import { MysqlDatabaseConfig } from './MysqlDatabaseConfig';
import { MysqlDatabaseConnection } from './MysqlDatabaseConnection';

/**
 * @class
 * Wrapper for the database connection
 */
export class MysqlDatabase {
/** @var {MysqlDatabaseConfig} dbConfig The database config instance  */
	public dbConfig : MysqlDatabaseConfig;
/** @var {MysqlDatabaseConnection} mysql The database connection instance.  */
	public mysql : MysqlDatabaseConnection;

	/**
	 * @constructor
	 * Initialise the database and create a connection
	 */
	constructor () {
		this.dbConfig = new MysqlDatabaseConfig();
		this.mysql = new MysqlDatabaseConnection(this.dbConfig.getDbConfig());
	}
}