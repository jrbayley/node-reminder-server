/**
 *
 * Basic interface of Mysql responses.
 * @interface
 *
 */
export interface IMysqlResponse {
	/** @var {number} fieldCount The number of fields in the response */
	fieldCount : number;

	/** @var {number} affectedRows The number of affected rows in the response */
	affectedRows : number;

	/** @var {number | string} insertId The id of the item inserted into the db */
	insertId : number | string;

	/** @var {number} serverStatus The current server status code */
	serverStatus : number;

	/** @var {number} warningCount The number of warnings generated when running the sql */
	warningCount : number;

	/** @var {string} message The message detailing the results of the sql */
	message : string;

	/** @var {boolean} protocol41 Protocol41 y/n */
	protocol41 : boolean;

	/** @var {number} changedRows The number of rows changed by the sql */
	changedRows : number;
}