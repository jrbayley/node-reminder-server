export interface IMysqlQueryResponse {
	/** @var {any[]} rows The returned data from the database */
	rows : any[];

	/** @var {string[]} fields The fields in the response */
	fields : string[];

	/** @var {number} rowCount The number of rows returned */
	rowCount : number;
}
