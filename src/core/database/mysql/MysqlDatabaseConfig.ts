import { Configuration } from '../../config/Configuration';

import mysql = require('mysql');
import moment = require('moment');
export class MysqlDatabaseConfig {
	/** @var {mysql.ConnectionConfig} dbConfig The current operational database config.  */
	private dbConfig : mysql.ConnectionConfig;

	 constructor () {
		 this.dbConfig = this.createDbConfig();
	 }
	/**
	 * @method
	 * Initialise the database configuration
	 * @returns {mysql.ConnectionConfig} The new database configuration.
	 */
	public createDbConfig = (): mysql.ConnectionConfig => {
		return {
			host: Configuration.DATABASE_HOSTNAME,
			user: Configuration.DATABASE_USERNAME,
			password: Configuration.DATABASE_PASSWORD,
			database: Configuration.DATABASE_SCHEMA,
			typeCast: (field, next) => {
				if (field.type === 'TINY' && field.length === 1) {
					return (field.string() === '1');
				} else if (field.type === 'DATE') {
					return moment(field.string());
				} else {
					return next();
				}
			}
		};
	};

	/**
	 * @method
	 * Return the database configuration
	 * @returns {mysql.ConnectionConfig} The current database configuration.
	 */
	public getDbConfig = (): mysql.ConnectionConfig => {
		return this.dbConfig;
	}

	/** @var {string} SCHEMA_PREFIX The prefix appied to all tables in the schema.  */
	public SCHEMA_PREFIX = '';

	/** @var {string} USER_TABLE The name of the database table where the users are stored.  */
	public USER_TABLE = 'user';

}