import { IMysqlQueryResponse } from './IMysqlQueryResponse';

/**
 * @class
 * Class for handling the response data from mysql
 */
export class MysqlQueryResponse implements IMysqlQueryResponse {
	/** @var {any[]} rows The returned data from the database */
	public rows : any[];

	/** @var {string[]} fields The fields in the response */
	public fields : string[];

	/** @var {number} rowCount The number of rows returned */
	public rowCount : number;

	/**
	 * @constructor
	 * Initialise the response class with the provided data
	 * @param {any[]} rows The rows returned
	 * @param {string[]} fields The fields returned
	 */
	constructor (rows: any[], fields: string[]) {
		this.rows = rows;
		this.fields = fields;
		this.rowCount = (rows) ? rows.length : 0;
	}
}