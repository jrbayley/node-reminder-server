
import moment from 'moment';
import mysql, { MysqlError } from 'mysql';

import { Configuration } from '../../../core/config/Configuration';
import logger from '../../logger/Log';
import startupLogger from '../../logger/StartupLog';
import { IMysqlResponse } from './models/IMysqlResponse';
import { MysqlQueryResponse } from './MysqlQueryResponse';

/**
 * @class
 * Create manage and interface to the database connecton
 */
export class MysqlDatabaseConnection {
/** @var {mysql.Pool} connectionPool The database connection pool */
	private  connectionPool : mysql.Pool;

/** @var {number} enqueueCount The number of times there has been an enqueue event */
	private  enqueueCount : number;

/** @var {number} releaseCount The number of times there has been a release event */
	private  releaseCount : number;

/** @var {number} queryCount The number of times there has been a query event */
	private  queryCount : number;

/** @var {number} insertCount The number of times there has been an insert event */
	private  insertCount : number;

/** @var {number} updateCount The number of times there has been an update event */
	private  updateCount : number;

/** @var {number} deleteCount The number of times there has been a delete event */
	private  deleteCount : number;

/**
 *
 * @constructor
 * @param config The database configuratiom
 */
	constructor (config: mysql.ConnectionConfig) {
		this.enqueueCount = 0;
		this.releaseCount = 0;
		this.connectionPool  = mysql.createPool(config);

		startupLogger.startup('MysqlDatabaseConnection : Connected to %s', Configuration.DATABASE_SCHEMA);

		this.connectionPool.on('acquire', (connection: mysql.PoolConnection): Promise<void> => {
			return this.connectionPoolAquired(connection);
		});

		this.connectionPool.on('enqueue', (sequence): Promise<void> => {
			return this.connectionPoolEnqueued();
		});

		this.connectionPool.on('release', (connection: mysql.PoolConnection): Promise<void> => {
			return this.connectionPoolReleased(connection);
		});

		this.connectionPool.on('error', (err: mysql.MysqlError): Promise<void> => {
			return this.connectionPoolError(err);
		});
	}

/**
 * @method
 * @private
 * Listener for connection events
 * @param {mysql.PoolConnection} connection the database connection
 * @returns {void} Empty promise.
 */
	private  connectionPoolAquired = (connection: mysql.PoolConnection): Promise<void> => {
		logger.silly('Connection pool thread id: %d acquired', connection.threadId);
		return Promise.resolve();
	}

/**
 * @method
 * @private
 * Listener for connection pool enqued events
 * @returns {void} Empty promise.
 */
	private  connectionPoolEnqueued = (): Promise<void> => {
		this.enqueueCount ++;
		logger.silly('Query is Waiting for available connection slot');
		return Promise.resolve();
	}

/**
 * @method
 * @private
 * Listener for connection pool released events
 * @param {mysql.PoolConnection} connection the database connection
 * @returns {void} Empty promise.
 */
	private  connectionPoolReleased = (connection: mysql.PoolConnection): Promise<void> => {
		this.releaseCount ++;
		logger.silly('Connection thread id: %d released', connection.threadId);
		return Promise.resolve();
	}

/**
 * @method
 * @private
 * Listener for error events
 * @param {mysql.PoolConnection} connection the database connection
 * @returns {void} Empty promise.
 */
	private connectionPoolError = (err: mysql.MysqlError): Promise<void> => {
		logger.debug('Connection pool error: %O', err);
		return Promise.resolve();
	}
/**
 * @method
 * @public
 * Prepare parse then run query sql on the database
 * @param {string} sql the query sql to run on the database connection
 * @param {any} args the arguments that are to be parsed into the sql
 * @returns {Promise<MysqlQueryResponse>} A mysql query response object containing the data from the query.
 */
	public query = (sql: string, args?: any): Promise<MysqlQueryResponse> => {
		this.queryCount ++;
		return new Promise((resolve, reject) => {
			const start = moment();
			this.connectionPool.query(sql, args, (err: MysqlError, rows: any, fields: any) => {
				const duration = moment().diff(start);
				logger.silly('Database.query : Executed query : time= %d ms', duration);
				if (err) {
					return reject(err);
				}
				resolve(new MysqlQueryResponse(rows, fields));
			});
		});
	}

/**
 * @method
 * @public
 * Prepare parse then run insert sql on the database
 * @param {string} sql the insert sql to run on the database connection
 * @param {any} args the arguments that are to be parsed into the sql
 * @returns {Promise<IMysqlResponse>} A mysql response for the query.
 */
	public insertQuery = (sql: string, args?: any): Promise<IMysqlResponse> => {
		this.insertCount ++;
		return new Promise((resolve, reject) => {
			const start = moment();
			this.connectionPool.query(sql, args, (err: Error, results: IMysqlResponse, fields: any[]) => {
				const duration = moment().diff(start);
				logger.silly('Database.query : Executed insert query : time= %d ms', duration);
				if (err) {
					logger.error('Da.insertQuery : Error inserting data : %O', err);
					return reject(err);
				}
				logger.silly('this.insertQuery : Inserted data : %O', results);
				resolve(results);
			});
		});
	}

/**
 * @method
 * @public
 * Prepare parse then run update sql on the database
 * @param {string} sql the update sql to run on the database connection
 * @param {any} args the arguments that are to be parsed into the sql
 * @returns {Promise<IMysqlResponse>} A mysql response for the query.
 */
	public updateQuery = (sql: string, args?: any): Promise<IMysqlResponse> => {
		this.insertCount ++;
		return new Promise((resolve, reject) => {
			const start = moment();
			console.log(sql, args)
			this.connectionPool.query(sql, args, (err: Error, results: IMysqlResponse, fields: any[]) => {
				const duration = moment().diff(start);
				logger.silly('Database.query : Executed update query : time= %d ms', duration);
				if (err) {
					logger.error('Database.query : %o', err)
					return reject(err);
				}
				resolve(results);
			});
		});
	}

/**
 * @method
 * @public
 * Prepare parse then run delete sql on the database
 * @param {string} sql the delete sql to run on the database connection
 * @param {any} args the arguments that are to be parsed into the sql
 * @returns {Promise<IMysqlResponse>} A mysql response for the query.
 */
	public deleteQuery = (sql: string, args?: any): Promise<IMysqlResponse> => {
		this.insertCount ++;
		return new Promise((resolve, reject) => {
			const start = moment();
			this.connectionPool.query(sql, args, (err: Error, results: IMysqlResponse, fields: any[]) => {
				const duration = moment().diff(start);
				logger.silly('Database.query : Executed delete query : time= %d ms', duration);
				if (err) {
					return reject(err);
				}
				resolve(results);
			});
		});
	}

/**
 * @method
 * @public
 * Close the database connection
 * @returns {Promise<void>} An empty.
 */
	public close = (): Promise<void> => {
		return new Promise((resolve, reject) => {
			this.connectionPool.end((error: Error) => {
				if (error) {
					return reject(error);
				}
				resolve();
			});
		});
	}
}