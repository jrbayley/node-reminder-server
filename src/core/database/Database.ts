import logger from '../logger/Log';
import { MysqlDatabase } from './mysql/MysqlDatabase';

/**
 * @class
 * Database service
 */
export class Database {

	/** @var {MysqlDatabase} mysqlDatabase The reminder mysql database */
	public static mysqlDatabase : MysqlDatabase;

	/** @method
	 * Initialise the connection to the database
	 * @returns {Promise<void>} returns an emptu promise.
	 */
	public static init = (): void => {
		logger.info('Database.init : Initialising MySql');
		Database.mysqlDatabase = new MysqlDatabase();
	}
}