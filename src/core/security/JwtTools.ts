import fs from 'fs';
import jwt, { SignOptions, VerifyOptions } from 'jsonwebtoken';

import { Configuration } from '../config/Configuration';

/**
 *
 * Tools for creating, validating and managing JWT tokens
 * @class
 *
 */
export class JwtTools {

	/** @var {string} privateKey Local cache of private key */
	private static privateKey : string;

	/** @var {string} publicKey Local cache of public key */
	private static publicKey : string;

	/** @var {string} issuer The JWT issuer string */
	private static issuer : string;

	/** @var {string} subject The JWT subject string */
	private static subject : string;

	/** @var {string} audience The JWT audience string */
	private static audience : string;

	/** @var {SignOptions} signOptions The JWT signing options object */
	private static signOptions : SignOptions;

	/** @var {VerifyOptions} verifyOptions The JWT verify options object */
	private static verifyOptions : VerifyOptions;


	/**
	 * @method
	 * @static
	 * Initialise the jwt components and keys.
	 */
	public static init = (): void => {
		JwtTools.issuer = Configuration.JWT_ISSUER;
		JwtTools.subject = Configuration.JWT_SUBJECT;
		JwtTools.audience = Configuration.JWT_AUDIENCE;
		JwtTools.privateKey  = fs.readFileSync(Configuration.PRIVATE_KEY, 'utf8');
		JwtTools.publicKey  = fs.readFileSync(Configuration.PUBLIC_KEY, 'utf8');
		JwtTools.signOptions =  {
			issuer:  JwtTools.issuer,
			subject:  JwtTools.subject,
			audience:  JwtTools.audience,
			expiresIn:  Configuration.JWT_EXPIRES_IN,
			algorithm:  Configuration.JWT_ALGORITHM
		};
		JwtTools.verifyOptions =  {
			issuer:  JwtTools.issuer,
			subject:  JwtTools.subject,
			audience:  JwtTools.audience,
			algorithms:  [Configuration.JWT_ALGORITHM]
		};
	}

	/**
	 * @method
	 * @static
	 * Return create the JWT and sign it.
	 */
	public static createJwt = (id: number, email: string, firstname: string, surname: string): string => {
		const payload = {
			littlereminder: {
				id: id,
				email: email,
				firstname: firstname,
				surname: surname,
				scope: 'reminder'
			}
		};
		return jwt.sign(payload, JwtTools.privateKey, JwtTools.signOptions);
	}

	/**
	 * @method
	 * @static
	 * Return decode the JWT and return the data.
	 */
	public static validateJwt = (token: string): string | object => {
		return jwt.verify(token, JwtTools.publicKey, JwtTools.verifyOptions);
	}

	/**
	 * @method
	 * @static
	 * Return the decoded token.
	 */
	public static decodeJwt = (token: string): string | object => {
		return jwt.decode(token, {complete: true});
	}

	/**
	 * @method
	 * @static
	 * Return the public key.
	 */
	public static getPublicKey = (): string => {
		return JwtTools.publicKey;
	}
}