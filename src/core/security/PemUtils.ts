import jwkToPem from 'jwk-to-pem';

/**
 * @class
 * Tools for obtaining the PEM files for managing encrypted or signed JWTs
 */
export class PemUtils {

	/**
	 * @method
	 * @static
	 * Return the service PEM.
	 * @returns {string} The pem file for decryption or signature verification
	 */
	public static jwkToPem = (): string => {
		const jwk = { alg: 'RS256', e: 'AQAB', kid: 'ZrWhQ/zv3d8LIlaP1neo3VRRy8kp2+2afYGpETPQ8XY=', kty: 'RSA', n: 'sWSuJJNvcT3vF1uMAT7_tweeXshrcEmBJGWsLPVp0yOlrtC73a7VzUanRR9BFsmqarRRmE8ATKKPrF2c4omUu3Tu7iJ_Ex6iWIUCb4ZgjgTwtp3ksbaszEjfJAz90CzeuRs9pBSG0nPCk4i04W5A1tzSlmOUY-RDhBgH4CqfHXkmHBK79oG5S0LD0dLVSww3He4oJdMon310BxTqVhQKBKDsyNDizth62vio-0xTtfjh8Bs1Jip7MFZcCV0sbWo3R4eFbpFkocpTl6D2y7uLy4HpRvXLnbVEpiOz55Tntfuh_eYi25VE4lctvFQPG2ARnqZhRAMH_Tf9oqV80VEAuQ', use: 'sig' };
		return jwkToPem((<any>jwk));
	}

	/**
	 * @method
	 * @static
	 * Return the user PEM.
	 * @returns {string} The pem file for decryption or signature verification
	 */
	public static userJwkToPem = (): string => {
		const jwk = { alg: 'RS256', e: 'AQAB', kid: '175D271ieJjGThpX6od4wAu5G4B42wAKVtADZhc5Ogk=', kty: 'RSA', n: 'spdTgZtHEAeu2y9cX48JU9hx8fFvIo7ExS70kpzyuyL6jmh27QCSGjaQU_aYiTTU3P31jwLgTnhK9Ofl5Qps8JfYAObpjozyqySduuWZNsXCiUkOTFSr6JUtJHCeeRW3rCVe8KpmQf4WA3dfrqWMfgiq_D8yNsdxt5QUBfgGdLCRWhhpAffU0IBN0r4js-OTLM7QgmgU8ibN83bqMp_-oBDuY4TE1hQ1D2Twbr_4Bchgkm5rwvHlM1N8KWhFCvrqQhZtPapKLKqJqMldZZ_KYeqNafcyHhqupn7_4mX46qzgE8JA_uXrM1Qu6Nvx8GSkXIbK01Nqc4yAvS6YDiD78w', use: 'sig' };
		return jwkToPem((<any>jwk));
	}
}