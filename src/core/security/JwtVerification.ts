import * as express from 'express';
import jwt from 'jsonwebtoken';

import { Configuration } from '../config/Configuration';
import logger from '../logger/Log';
import { JwtServiceVerification } from './JwtServiceVerification';
import { JwtTools } from './JwtTools';

/**
 *
 * JWT Verification service for this app.
 * @class
 *
 */
export class JwtVerification {

	/** @var {JwtServiceVerification} JwtServiceVerification The jwt verification service. */
	private jwtServiceVerification : JwtServiceVerification;

	/**
	 * @constructor
	 */
	constructor () {
		this.jwtServiceVerification = new JwtServiceVerification();
	}

	/**
	 * @method
	 * verify the JWT outputting any results to the calling service in http response.
	 */
	public verifyJwt = (req: express.Request, res: express.Response, next: express.NextFunction): any => {
		if (this.jwtServiceVerification.endpointSecurity(req.url)) {
			next();
		} else {
			const tokenRaw = req.headers['authorization']; // tslint:disable-line:no-string-literal
			if (tokenRaw) {
				const tokenParts = tokenRaw.split(' ');
				if (tokenParts.length === 2) {
					const token = tokenParts[1];
					const cert = JwtTools.getPublicKey();
					if (cert) {
						jwt.verify(token, cert, (err: Error, decodedJwt: any) => {
							logger.silly('JwtVerification.verifyJwt: Using the following JWT: %O', decodedJwt);
							if (err) {
								logger.error('JwtVerification.verifyJwt: Unable to verify the JWT : %O', err.message);
								res.status(401);
								res.send('Forbidden : ' + err);
								res.end();
							} else if (! this.jwtServiceVerification.checkForJwtScope(decodedJwt, Configuration.REMINDER_REQUIRED_JWT_SCOPE)) {
								res.status(401);
								res.send('Forbidden : The required scope parameters [' + Configuration.REMINDER_REQUIRED_JWT_SCOPE + '] were not set in token.');
								res.end();
							} else {
								(<any>req).user = decodedJwt.littlereminder;
								next();
							}
						});
					} else {
						res.status(401);
						res.send('Forbidden : Invalid PEM certificate');
						res.end();
					}
				} else {
					res.status(401);
					res.send('Forbidden : Invalid header bearer token format');
					res.end();
				}
			} else {
				res.status(401);
				res.send('Forbidden : No JWT provided');
				res.end();
			}
		}
	}
}