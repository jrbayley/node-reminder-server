import { Configuration } from '../config/Configuration';
import logger from '../logger/Log';

/**
 *
 * JWT Verification to extract data and validate for use with this app.
 * @class
 *
 */
export class JwtServiceVerification {

	/**
	 * @constructor
	 */
	constructor () {
		logger.info('JwtServiceVerification.constructor : Initialising JWT Verification.');
	}

	/**
	 * @method
	 * check that the required JWT scope is present in the JWT.
	 * @param {any} decodedJwt The JWT to check for scope.
	 * @param {string} requiredScope The scope to check for.
	 * @returns {boolean} True if the scope is present
	 */
	public checkForJwtScope = (decodedJwt: any, requiredScope: string): boolean => {
		if (decodedJwt && decodedJwt.littlereminder.scope) {
			return decodedJwt.littlereminder.scope.includes(requiredScope);
		} else {
			return false;
		}
	}

	/**
	 * @method
	 * Test to see if the endpoint url called is one of the configured unsecured endpoints.
	 * @param {string} endpointToCheck The endpoint to check.
	 * @returns {boolean} returns true if the endpoint is allowed to be accessed without a JWT
	 */
	public endpointSecurity = (endpointToCheck: string): boolean => {
		for (const endpoint of Configuration.UNSECURE_ENDPOINTS) {
			logger.silly('JwtServiceVerification.endpointSecurity : Checking to see if %s matched unsecured list entry %s', endpointToCheck, endpoint);
			if (endpointToCheck.indexOf(endpoint) === 0)  {
				logger.debug('JwtServiceVerification.endpointSecurity : Allowing unsecured endpoint %s.', endpoint);
				return true;
			}
		}
		logger.debug('JwtServiceVerification.endpointSecurity : Starting security for secured endpoint %s.', endpointToCheck);
		return false;
	}
}