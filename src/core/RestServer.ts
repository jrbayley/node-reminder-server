
import { Configuration } from '../core/config/Configuration';
import { AccountController } from '../restControllers/AccountController';
import { HealthController } from '../restControllers/HealthController';
import { ReminderController } from '../restControllers/ReminderController';
import { ReminderEmailController } from '../restControllers/ReminderEmailController';
import { ReminderPageController } from '../restControllers/ReminderPageController';
import { ReminderPeriodController } from '../restControllers/ReminderPeriodController';
import { ReminderPlanController } from '../restControllers/ReminderPlanController';
import { SigninController } from '../restControllers/SigninController';
import { App } from './App';

/**
 *
 * Create a rest eserver to instantiate the endpoints and handle data and app requests and storage.
 * @class
 *
 */
export class RestServer {
	/** @var @static {App} server A reference to the express application. */
	public static server : App;

	/**
	 * @method
	 * @static
	 * Initialise the server and any components.
	 * @returns { Promise<void> } void Promise used to allow integration with any async components.
	 */
	public static init = (): void => {
		// Instantiate the app passing in the rest controllers
		RestServer.server = new App(
			[
				new AccountController(),
				new HealthController(),
				new SigninController(),
				new ReminderController(),
				new ReminderPeriodController(),
				new ReminderPlanController(),
				new ReminderPageController(),
				new ReminderEmailController()
			],
			Configuration.SERVER_PORT,
		);
	}

	/**
	 * @method
	 * @static
	 * Start the rest service App webservice and listen for any requests.
	 */
	public static listen = (): void => {
		RestServer.server.listen();
	}
}
