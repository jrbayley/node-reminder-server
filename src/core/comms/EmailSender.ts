import nodemailer = require('nodemailer');
import Mail = require('nodemailer/lib/mailer');
import SMTPTransport = require('nodemailer/lib/smtp-transport');
import { MailOptions } from 'nodemailer/lib/json-transport';

import { Configuration } from '../config/Configuration';

/**
 * @class
 * Comms class to handle the sending of emails.
 */
export class EmailSender {

	/** @var {Mail} transport Reference to the smtp transport configuration */
	private transport : Mail;

	/**
	 * @constructor
	 * Initialise the email sender class
	 */
	 constructor () {
		 const options: SMTPTransport.Options = {
			host: Configuration.SMTP_HOST,
			port: Configuration.SMTP_PORT,
			secure: false // true for 465, false for other ports
		 };
		this.transport = nodemailer.createTransport(options);
	 }

	/**
	 * @method
	 * @public
	 * Handle the sending of an email to the configured smtp service
	 * @param {MailOptions} mailOptions The mail options object containing the message details
	 * @returns {SMTPTransport.SentMessageInfo} Returns the smtp response
	 */
	 public sendEmail = (mailOptions: MailOptions): Promise<SMTPTransport.SentMessageInfo> => {
		return new Promise((resolve, reject): void => {
			return this.transport.sendMail(mailOptions, (err: Error, info: SMTPTransport.SentMessageInfo): void => {
				if (err) {
					reject(err);
				  } else {
					resolve(info);
				  }
			});
		});
	}

	/**
	 * @method
	 * @public
	 * Handle the sending of an email to the configured smtp service
	 * @param {MailOptions} mailOptions The mail options object containing the message details
	 * @returns {SMTPTransport.SentMessageInfo} Returns the smtp response
	 */
	public emailSendResult = (smtpMessageInfo: SMTPTransport.SentMessageInfo): boolean => {
		return (smtpMessageInfo.messageId && smtpMessageInfo.messageId !== '');
	}
}