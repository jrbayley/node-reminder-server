import { EmailSender } from './EmailSender';

/**
 * @class
 * Manage any communication to external sources
 */
export class Comms {

	/** @var {EmailSender} emailSender The local rest request instance */
	public static emailSender : EmailSender;

	/**
	 * @method
	 * @public
	 * @static
	 * Initialise the local static reference with a new Rest Request instance
	 */
	public static init = (): void => {
		Comms.emailSender = new EmailSender();
	}
}