import { Algorithm } from 'jsonwebtoken';

require('dotenv').config();

/**
 *
 * Configuration parameters for the project..
 * @class
 */

export class Configuration {

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} VERSION  The version text etc
	 */
	public static VERSION = '1.0.0';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} NODE_ENV  The environment that the service is running on to control logging etc
	 */
	public static NODE_ENV : string 		= process.env.NODE_ENV || 'development';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} LOGLEVEL The loglevel of the app.
	 */
	public static LOGLEVEL					= process.env.LOGLEVEL 					|| 'info';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} SERVER_PORT The port number that ths app should run on
	 */
	public static SERVER_PORT : number		= parseInt(process.env.SERVER_PORT)		|| 3000;

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} SECURE_MODE The port number that ths app should run on
	 */
	public static SECURE_MODE : string		= process.env.SECURE_MODE				|| 'true';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} BASE_URI The domain name of the server that this process is running on
	 */
	public static BASE_URI 					= '/rem';

	/** The database connection details */

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} DATABASE_HOSTNAME The location of the mysql database
	 */
	public static DATABASE_HOSTNAME 		= process.env.DATABASE_HOSTNAME 		|| 'localhost';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} DATABASE_USERNAME The username for the mysql database
	 */
	public static DATABASE_USERNAME 		= process.env.DATABASE_USERNAME 		|| 'reminder';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} DATABASE_PASSWORD The password for the mysql database
	 */
	public static DATABASE_PASSWORD 		= process.env.DATABASE_PASSWORD 		|| 'reminderTomato13@';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} DATABASE_SCHEMA The schema for monitoring
	 */
	public static DATABASE_SCHEMA 			= process.env.DATABASE_SCHEMA 			|| 'littlereminder.co.uk';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} UNSECURED_ENDPOINTS The endpoints that we allow without security
	 */
	public static readonly UNSECURE_ENDPOINTS : string[]							= ['/rem/signin', '/rem/public', '/health', '/rem/health', '/rem/reminderPage', '/rem/recover', '/rem/reset', '/rem/register', '/rem/emailAvailable'];

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} REMINDER_REQUIRED_JWT_SCOPE The scope that must be in the jwt to enable access to service
	 */
	public static readonly REMINDER_REQUIRED_JWT_SCOPE : string						= 'reminder';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} JWT_ISSUER The jwt issuer
	 */
	public static readonly JWT_ISSUER : string										= 'Little Reminder';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} JWT_SUBJECT The jwt subject
	 */
	public static readonly JWT_SUBJECT : string										= 'reminder@littlereminder';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} JWT_AUDIENCE The jwt audience
	 */
	public static readonly JWT_AUDIENCE : string									= 'https://littlereminder.co.uk';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} JWT_EXPIRES_IN The jwt expiry time
	 */
	public static readonly JWT_EXPIRES_IN : string									= '12h';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} JWT_ALGORITHM The jwt signing algorith
	 */
	public static readonly JWT_ALGORITHM : Algorithm								= 'RS512';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} PUBLIC_KEY The path and filename of the public key
	 */
	public static readonly PUBLIC_KEY : string										= './public.key';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} PRIVATE_KEY The path and filename of the private key
	 */
	public static readonly PRIVATE_KEY : string										= './private.key';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} SMTP_HOST The hostname for the smtp server
	 */
	public static readonly SMTP_HOST : string										= 'relay.plus.net';

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {number} SMTP_PORT The port smtp server
	 */
	public static readonly SMTP_PORT : number										= 587;

	/**
	 * @var
	 * @method
	 * @public
	 * @static
	 * @returns {string} EMAIL_FROM The email address of the reminder service
	 */
	public static readonly EMAIL_FROM : string										= 'reminders@littlereminder.co.uk'
}