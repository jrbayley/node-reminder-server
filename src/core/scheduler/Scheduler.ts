import schedule from 'node-schedule';

import { Listeners } from '../listener/Listeners';
import logger from '../logger/Log';

/**
 *
 * Create The app scheduler.
 * @class
 *
 */
export class Scheduler {

	/** @var {any | ReturnType<typeof setTimeout>} oneMinute The sheduler one minute call  */
	private oneMinute : any | ReturnType<typeof setTimeout>;

	/** @var {any | ReturnType<typeof setTimeout>} fiveMinute The sheduler five minute call  */
	private fiveMinute : any | ReturnType<typeof setTimeout>;

	/** @var {any | ReturnType<typeof setTimeout>} tenMinute The sheduler ten minute call  */
	private tenMinute : any | ReturnType<typeof setTimeout>;

	/** @var {any | ReturnType<typeof setTimeout>} thirtyMinute The sheduler thirty minute call  */
	private thirtyMinute : any | ReturnType<typeof setTimeout>;

	/** @var {any | ReturnType<typeof setTimeout>} hourly The sheduler hourly minute call  */
	private hourly : any | ReturnType<typeof setTimeout>;

	/** @var {any | ReturnType<typeof setTimeout>} daily The sheduler daily minute call  */
	private daily : any | ReturnType<typeof setTimeout>;

	/**
	 * @constructor
	 * When initialising run the stop schedules method to clean up any schedules.
	 */
	constructor () {
		this.stopSchedules();
	}

	/**
	 * @method initialise
	 * Initialise the schedules and call a run of all schedules at startup
	 */
	public initialise = (): Promise<void> => {
		return Promise.resolve()
		.then(() => {
			this.oneMinute = schedule.scheduleJob('*/1 * * * *', () => {
				logger.info('Scheduler: running one minute schedules');
				this.oneMinuteSchedule();
			});

			this.fiveMinute = schedule.scheduleJob('*/5 * * * *', () => {
				logger.info('Scheduler: running five minute schedules');
				this.fiveMinuteSchedule();
			});

			this.tenMinute = schedule.scheduleJob('*/10 * * * *', () => {
				logger.info('Scheduler: running ten minute schedules');
				this.tenMinuteSchedule();
			});

			this.thirtyMinute = schedule.scheduleJob('*/30 * * * *', () => {
				logger.info('Scheduler: running thirty minute schedules');
				this.thirtyMinuteSchedule();
			});

			this.hourly = schedule.scheduleJob('00 */1 * * *', () => {
				logger.info('Scheduler: running on the hour schedules');
				this.hourlySchedule();
			});

			this.daily = schedule.scheduleJob('0 8 * * *', () => {
				logger.info('Scheduler: running daily 8am schedules');
				this.dailySchedule();
			});

			logger.debug('Scheduler: Started Schedules.');
		});
	}

	/**
	 * @method
	 * The one minute schedule
	 * @returns {Promise<void>} An empty promise
	 */
	private oneMinuteSchedule = (): Promise<void> => {
		logger.info('Scheduler.oneMinuteSchedule : Running tasks');
		return Listeners.oneMinuteSchedule.publish();
	}

	/**
	 * @method
	 * The five minute schedule
	 * @returns {Promise<void>} An empty promise
	 */
	private fiveMinuteSchedule = (): Promise<void> => {
		logger.info('Scheduler.fiveMinuteSchedule : Running tasks');
		return Listeners.fiveMinuteSchedule.publish();
	}

	/**
	 * @method
	 * The ten minute schedule
	 * @returns {Promise<void>} An empty promise
	 */
	private tenMinuteSchedule = (): Promise<void> => {
		logger.info('Scheduler.tenMinuteSchedule : Running tasks');
		return Listeners.tenMinuteSchedule.publish();
	}

	/**
	 * @method
	 * The thirty minute schedule
	 * @returns {Promise<void>} An empty promise
	 */
	private thirtyMinuteSchedule = (): Promise<void> => {
		logger.info('Scheduler.thirtyMinuteSchedule : Running tasks');
		return Listeners.thirtyMinuteSchedule.publish();
	}

	/**
	 * @method
	 * The hourly schedule
	 * @returns {Promise<void>} An empty promise
	 */
	private hourlySchedule = (): Promise<void> => {
		logger.info('Scheduler.hourlySchedule : Running tasks');
		return Listeners.hourlySchedule.publish();
	}

	/**
	 * @method
	 * The daily schedule
	 * @returns {Promise<void>} An empty promise
	 */
	private dailySchedule = (): Promise<void> => {
		logger.info('Scheduler.dailySchedule : Running tasks');
		// return Listeners.dailySchedule.publish();
		return;
	}

	/**
	 * @method
	 * Reset all the schedules
	 * @returns {Promise<void>} An empty promise
	 */
	private stopSchedules = (): void => {
		if (this.oneMinute) {
			this.oneMinute.clearInterval();
		}
		if (this.fiveMinute) {
			this.fiveMinute.clearInterval();
		}
		if (this.tenMinute) {
			this.tenMinute.clearInterval();
		}
		if (this.thirtyMinute) {
			this.thirtyMinute.clearInterval();
		}
		if (this.hourly) {
			this.hourly.clearInterval();
		}
		if (this.daily) {
			this.daily.clearInterval();
		}
	}

	/**
	 * @method
	 * Run all the schedules
	 * @returns {Promise<void>} An empty promise
	 */
	private runAll = async (): Promise<void> => {
		await this.oneMinuteSchedule();
		await this.fiveMinuteSchedule();
		await this.tenMinuteSchedule();
		await this.thirtyMinuteSchedule();
		await this.hourlySchedule();
		await this.dailySchedule();
	}

}