/**
 * @class
 * Collection of all the app listeners
 */

import SMTPTransport from 'nodemailer/lib/smtp-transport';

import { ReminderEmailType } from '../../reminder/email/ReminderEmailType';
import { Publisher } from './Publisher';

export class Listeners {
		/** @var emailSent Signal that an email was successfully sent */
		public static emailSent : Publisher<(SMTPTransport.SentMessageInfo)>;

		/** @var reminderEmailSent Signal that areminder email was sent */
		public static reminderEmailSent : Publisher<(ReminderEmailType)>;

		/** @var oneMinuteSchedule Signal that the one minute scheduled has fired */
		public static oneMinuteSchedule : Publisher<(void)>;

		/** @var fiveMinuteSchedule Signal that the five minute scheduled has fired */
		public static fiveMinuteSchedule : Publisher<(void)>;

		/** @var tenMinuteSchedule Signal that the ten minute scheduled has fired */
		public static tenMinuteSchedule : Publisher<(void)>;

		/** @var thirtyMinuteSchedule Signal that the thirty minute scheduled has fired */
		public static thirtyMinuteSchedule : Publisher<(void)>;

		/** @var HourlySchedule Signal that the hourly scheduled has fired */
		public static hourlySchedule : Publisher<(void)>;

		/** @var DailySchedule Signal that the daily scheduled has fired */
		public static dailySchedule : Publisher<(void)>;

	/**
	 * @method
	 * @static
	 * @public
	 * Initialise all the listeners to allow publish of add state
	 */
	public static init = (): void => {
		Listeners.emailSent = new Publisher();
		Listeners.reminderEmailSent = new Publisher();
		Listeners.oneMinuteSchedule = new Publisher();
		Listeners.fiveMinuteSchedule = new Publisher();
		Listeners.tenMinuteSchedule = new Publisher();
		Listeners.thirtyMinuteSchedule = new Publisher();
		Listeners.hourlySchedule = new Publisher();
		Listeners.dailySchedule = new Publisher();
	}
}