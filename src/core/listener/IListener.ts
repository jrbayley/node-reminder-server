import { IValueListener } from './IValueListener';

export interface IListener<T> extends IValueListener<T,Promise<void>> {
	//
}