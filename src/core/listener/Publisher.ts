import _ from 'lodash';

import { IListener } from './IListener';

/**
 * @class
 * Publisher generic class
 */
export class Publisher<T> {

	/** @var {IListener<T>[]} subscribers An array of promised based subscribers */
	private subscribers : IListener<T>[] = [];

	constructor() {
		//
	}

	/**
	 * @method
	 * @public
	 * Return the number of subscribers on this publisher
	 * @returns {number} The number of subscribers
	 */
	public numListeners = (): number => {
		return this.subscribers.length;
	};

	/**
	 * @method
	 * @public
	 * Subscribe the passed listener
	 * @param {IListener<T>} listener The listener to subscribe to the publisher
	 */
	public subscribe = (listener: IListener<T>): void => {
		this.unsubscribe(listener);
		this.subscribers.push(listener);
	};

	/**
	 * @method
	 * @public
	 * Unsubscribe the passed listener from the publisher
	 * @param {IListener<T>} listener The listener to subscribe
	 */
	public unsubscribe = (listener: IListener<T>): void => {
		_.pull(this.subscribers, listener);
	};

	/**
	 * @method
	 * @public
	 * Fire an on change event to the subscribed listeners
	 * @param {T} date The date to send to all subscribed listeners
	 */
	public publish = async (data?: T): Promise<void> => {
		const listeners: Promise<void>[] = [];
		for (const listener of this.subscribers) {
			listeners.push(listener(data));
		}
		await Promise.all(listeners);
	};
}