import { Users } from '../../reminder/user/Users';

/**
 *
 * Create a class for storing local cache of data that may be used for logic or lookups regularly.
 * @class
 *
 */
export class AppData {
	/** @var {User[]} users Local cache of users */
	public static users : Users;

	/**
	 * Initialise the AppData cache and refresh
	 * @method
	 * @returns {Promise<void} An Empty promise
	 */
		public static init = (): void => {
			AppData.users = new Users();
		}
}