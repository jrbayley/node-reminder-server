import moment from 'moment';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

import { Database } from '../../core/database/Database';
import { IMysqlResponse } from '../../core/database/mysql/models/IMysqlResponse';
import logger from '../../core/logger/Log';
import { EmailUtils } from '../email/EmailUtils';
import { ReminderEmailer } from '../email/ReminderEmailer';
import { Schema } from '../Schema';
import { IUser } from './IUser';
import { UserQueries } from './UserQueries';
import { UserTools } from './UserTools';

/**
 * Handle the user password rese
 */

export class UserPasswordReset {

	/**
	 * Handle the user password recovery email and storage of code
	 * @method resetPasswordFor
	 * @public
	 * @static
	 * @async
	 * @param {string} email The user email to recover the password
	 * @returns {Promise<void>} returns an empty promise.
	 */
	public static resetRequestFor = async(email: string): Promise<void> => {
		try {
			const user = await UserTools.getUserByEmail(email);
			if (user) {
				user.user_forgotten_password_code = UserPasswordReset.generateResetCode();
				user.user_forgotten_password_time = moment().toDate();
				logger.debug('%o', user);
				const mySqlResponse: IMysqlResponse = await UserPasswordReset.storePasswordResetData(user);
				if (mySqlResponse.changedRows > 0) {
					const template: HandlebarsTemplateDelegate<any> = await EmailUtils.prepareEmailTemplate('emails/recovery_template.html');
					UserPasswordReset.sendRecoveryEmail(user, template(user));
				}
			} else {
				return;
			}
		} catch (error) {
			logger.error('UserPasswordReset.resetPasswordFor : Error resetting user passwprd : %s', error.message);
		}
	}

	/**
	 * Send the recovery email
	 * @method sendRecoveryEmail
	 * @private
	 * @static
	 * @param {IUser} user The user to send the email to
	 * @param {string} template The template to parse the user object into
	 * @returns {Promise<void>} returns an empty promise
	 */
	private static sendRecoveryEmail = (user: IUser, template: string): Promise<void> => {
		const mail = ReminderEmailer.createReminderEmail(user.user_email, 'Reminder: Lost Password Recovery', template);
		// console.log(template.toString());
		ReminderEmailer.sendReminderEmail(mail)
		.then((response: SMTPTransport.SentMessageInfo) => {
			console.log(response);
		});
		return;
	}

	/**
	 * Generate the password reset code for the user email
	 * @method generateResetCode
	 * @public
	 * @static
	 * @returns {string} returns a string with the reset code.
	 */
	public static generateResetCode = (): string => {
		return Math.floor(100000 + Math.random() * 900000).toString();
	}

	/**
	 * Save the password reset data
	 * @method storePasswordResetData
	 * @private
	 * @static
	 * @async
	 * @param {IUser} user The user data to update to database
	 * @returns {Promise<IMysqlResponse>} returns a promise containing the results of the update.
	 */
	private static storePasswordResetData = (user: IUser): Promise<IMysqlResponse> => {
		return Database.mysqlDatabase.mysql.updateQuery(UserQueries.updatePasswordReset, [Schema.USERS_TABLE, user.user_forgotten_password_code, user.user_forgotten_password_time, user.user_id]);
	}
}