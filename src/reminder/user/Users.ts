import logger from '../../core/logger/Log';
import { JwtTools } from '../../core/security/JwtTools';
import { IUser } from './IUser';
import { IUserValidationResponse } from './IUserValidationResponse';
import { User } from './User';
import { UserTools } from './UserTools';

/**
 * @class
 * Users data central location for accessing users from database or cache
 */
export class Users {

	constructor() {
		//
	}

	/**
	 * @method
	 * See if the user exists using the provided details
	 * @param {string} userId The user id to look up
	 * @param {string} password The password to verify the user signin against
	 * @returns {IUserValidationResponse} Returns the results and if necessary the data of the user signin.
	 */
	public getUserById = async (userId: string, password: string): Promise<IUserValidationResponse> => {
		let success = false;
		let message = 'Invalid Password';
		let token = undefined;
		let userResponse: IUser = undefined;
		const userData: IUser = await UserTools.getUser(userId);
		if (userData) {
			const user: User = new User(userData);
			if (user.canSignIn(password).success) {
				success = true;
				message = 'ok';
				token = JwtTools.createJwt(user.user_id, user.user_email, user.user_firstname, user.user_surname);
				userResponse = userData;
			}
		}
		return {
			success: success,
			message: message,
			token: token,
			user: userResponse
		};
	}


	/**
	 * @method
	 * See if the user exists using the provided details
	 * @param {string} email Either the username or email to look up
	 * @param {string} password The password to verify the user signin against
	 * @returns {IUserValidationResponse} Returns the results and if necessary the data of the user signin.
	 */
	public getUserByEmail = async (email: string, password: string): Promise<IUserValidationResponse> => {
		let success = false;
		let message = 'Invalid email or password';
		let token = undefined;
		let userResponse: IUser = undefined;
		const userData: IUser = await UserTools.getUserByEmail(email);
		if (userData) {
			const user: User = new User(userData);
			const passwordCheck: IUserValidationResponse = user.canSignIn(password);
			logger.debug('User.canSignIn : Password check %o', passwordCheck);
			if (passwordCheck && passwordCheck.success) {
				success = true;
				message = 'ok';
				token = JwtTools.createJwt(user.user_id, user.user_email, user.user_firstname, user.user_surname);
				userResponse = userData;
			} else {
				message = passwordCheck.message;
			}
		}
		return {
			success: success,
			message: message,
			token: token,
			user: userResponse
		};
	}
}