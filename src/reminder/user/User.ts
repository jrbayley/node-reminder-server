import { token } from 'morgan';
import Password from 'node-php-password';

import { IUser } from './IUser';
import { IUserValidationResponse } from './IUserValidationResponse';

export class User implements IUser {
	/** @var {number} user_id Surrogate PK unique identifier used to link the user */
	public user_id : number;

	/** @var {string} user_email Users email address */
	public user_email : string; //

	/** @var {boolean} user_suspended  User account is suspended by administrators */
	public user_suspended : boolean;

	/** @var {string} user_firstname The user first / christian name */
	public user_firstname : string;

	/** @var {string} user_surname The user Surname / Family name */
	public user_surname : string;

	/** @var {string} user_password The encrypted user password */
	public user_password : string;

	/** @var {Date} user_password_expires_on User password expiry. Users cannot log in after this date. */
	public user_password_expires_on : Date;

	/** @var {Date} user_created_on The timestamp of user creation / registration */
	public user_created_on : Date;

	/** @var {boolean} user_locked_out Account locked out - security breach - User resettable. */
	public user_locked_out : boolean;

	/** @var {Date} user_last_login The time that the user last logged in [Form login only] */
	public user_last_login : Date;

	/** @var {number} user_failed_logins The number of failed login attempts */
	public user_failed_logins : number;

	/** @var {string} user_forgotten_password_code Encrypted value that is sent to the user to prove they received an email */
	public user_forgotten_password_code : string;

	/** @var {Date} user_forgotten_password_time The time  that the user entered the recover password system */
	public user_forgotten_password_time : Date;

	/** @var {boolean} user_verified  Indicates that the user has successfully completed the email validation verifying the registration email address */
	public user_verified : boolean;

	/** @var {string} user_image_filename The users image if set, */
	public user_image_filename : string;

	/**
	 * @constructor
	 * @param {IUser} user The IUser interface to initlialise with
	 */

	constructor(user: IUser) {
		this.user_id = user.user_id;
		this.user_email = user.user_email;
		this.user_suspended = user.user_suspended;
		this.user_firstname = user.user_firstname;
		this.user_surname = user.user_surname;
		this.user_password = user.user_password;
		this.user_password_expires_on = user.user_password_expires_on;
		this.user_created_on = user.user_created_on;
		this.user_locked_out = user.user_locked_out;
		this.user_last_login = user.user_last_login;
		this.user_failed_logins = user.user_failed_logins;
		this.user_forgotten_password_code = user.user_forgotten_password_code;
		this.user_forgotten_password_time = user.user_forgotten_password_time;
		this.user_verified = user.user_verified;
		this.user_image_filename = user.user_image_filename;
	}

	/**
	 * @method
	 * User is allowed to sign in
	 * @param {string} password The user password to validate
	 * @returns {boolean} returns true if signin is permitted
	 */
	public canSignIn = (password: string): IUserValidationResponse => {
		if (! this.passwordValid(password)) {
			return this.generateValidationResponse(false, 'Invalid email or password');
		}
		if (! this.user_verified) {
			return this.generateValidationResponse(false, 'Email not verified');
		}
		if (this.user_suspended) {
			return this.generateValidationResponse(false, 'Account Suspended');
		}
		if (this.user_locked_out || this.user_failed_logins > 10) {
			return this.generateValidationResponse(false, 'Account Locked Out');
		}
		if (this.user_password_expires_on < new Date()) {
			return this.generateValidationResponse(false, 'Password Expired');
		}
		return this.generateValidationResponse(true, 'Ok');
	}

	/**
	 * @method
	 * User password validation check
	 * @returns {boolean} returns true if the user password is valid
	 */
	public passwordValid = (password: string): boolean => {
		return Password.verify(password, this.user_password);
	}

	/**
	 * @method
	 * User signin validation check
	 * Used to generate the response that the signin check will create
	 * @param {boolean} success
	 * @param {string} message
	 * @returns {IUserValidationResponse} returns the status of the user signin
	 */
	private generateValidationResponse = (success: boolean, message: string): IUserValidationResponse => {
		const user = (success) ? User.asJson(this) : undefined;
		return {
				success: success,
				message: message,
				token: undefined,
				user: user
		};
	}

	/**
	 * @method
	 * User interface generator
	 * Used to generate the response that the signin check will create
	 * @returns {IUser} returns an IUser interface from a user
	 */
	public static asJson = (user: User): IUser => {
		return {
			user_id: user.user_id,
			user_email: user.user_email,
			user_suspended: user.user_suspended,
			user_firstname: user.user_firstname,
			user_surname: user.user_surname,
			user_password: '',
			user_password_expires_on: user.user_password_expires_on,
			user_created_on: user.user_created_on,
			user_locked_out: user.user_locked_out,
			user_last_login: user.user_last_login,
			user_failed_logins: user.user_failed_logins,
			user_forgotten_password_code: user.user_forgotten_password_code,
			user_forgotten_password_time: user.user_forgotten_password_time,
			user_verified: user.user_verified,
			user_image_filename: user.user_image_filename
		};
   }
}