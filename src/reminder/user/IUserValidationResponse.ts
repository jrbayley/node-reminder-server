import { IUser } from './IUser';

/**
 * @interface
 * This is the interface for user signin validation from the backend server.
 */
export interface IUserValidationResponse {
	/** @var {boolean} success Was the user validation successful */
	success : boolean;

	/** @var {string} message Information on the failure of validation */
	message : string;

	/** @var {string} token If the validation succeeded then there will be a JWT to use to query the backend with */
	token : string;

	/** @var {IUser} user  If the validation succeeded then there will be user data for any UI customisation */
	user : IUser | undefined;
}