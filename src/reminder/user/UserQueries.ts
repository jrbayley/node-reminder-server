export class UserQueries {

	/**
	 * Signin user using email SQL with bind points
	 * @method
	 * @public
	 * @static
	 * @returns {string} The sql to select the user
	 */
	public static signinUser = 'SELECT * FROM ?? WHERE user_email = ?';

	/**
	 * Signin user using id SQL with bind points
	 * @method
	 * @public
	 * @static
	 * @returns {string} The sql to select the user
	 */
	public static signinUserById = 'SELECT * FROM ?? WHERE user_id = ?';

	/**
	 * Update user password sql with bind points
	 * @method
	 * @public
	 * @static
	 * @returns {string} The sql to update the user password
	 */
	public static updatePassword = 'UPDATE ?? SET user_password = ? WHERE user_id = ?';

	/**
	 * Update user password reset date sql with bind points
	 * @method
	 * @public
	 * @static
	 * @returns {string} The sql to store the password reset data
	 */
	public static updatePasswordReset = 'UPDATE ?? SET user_forgotten_password_code = ?, user_forgotten_password_time = ? WHERE user_id = ?';


	/**
	 * @method
	 * @public
	 * @static
	 * @returns {string} The SQL to insert a user with bind placeholder for the table name and user object
	 */
	public static insertUser = 'INSERT INTO ?? SET ?';

	/**
	 * @method
	 * @public
	 * @static
	 * @returns {string} The SQL to update a user with bind placeholder for the table name and user object
	 */
	public static updateUserVerification = 'UPDATE ?? SET user_verified = ? WHERE user_id = ? ';


	/**
	 * @method
	 * @public
	 * @static
	 * @returns {string} The SQL to update a user with bind placeholder for the table name and user object
	 */
	public static updateUserLockout = 'UPDATE ?? SET user_locked_out = ? WHERE user_id = ? ';

	/**
	 * @method
	 * @public
	 * @static
	 * @returns {string} The SQL to update a user with bind placeholder for the table name and user object
	 */
	public static updateUserSuspension = 'UPDATE ?? SET user_suspended = ? WHERE user_id = ? ';
}