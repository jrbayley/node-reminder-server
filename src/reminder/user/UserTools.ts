import moment from 'moment';
import Password from 'node-php-password';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

import { Database } from '../../core/database/Database';
import { IMysqlResponse } from '../../core/database/mysql/models/IMysqlResponse';
import logger from '../../core/logger/Log';
import { IAccountResponse } from '../../restControllers/restRequests/IAccountResponse';
import { IPasswordResetRequest } from '../../restControllers/restRequests/IPasswordResetRequest';
import { IRegistrationRequest } from '../../restControllers/restRequests/IRegistrationRequest';
import { EmailUtils } from '../email/EmailUtils';
import { ReminderEmailer } from '../email/ReminderEmailer';
import { Schema } from '../Schema';
import { IUser } from './IUser';
import { UserPasswordReset } from './UserPasswordReset';
import { UserQueries } from './UserQueries';

/**
 * User retrieval tools for interfacing to the database
 * @class
 */
export class UserTools {

	/**
	 * Lookup the user in the database using the provided email
	 * @method getUser
	 * @public
	 * @static
	 * @async
	 * @param {string} userId The users id to look up
	 * @returns {Promise<IUser | undefined>} returns the user or if lookup failed undefined.
	 */
	public static getUser = async (userId: string): Promise<IUser | undefined> => {
		try {
			const mysqlQueryResponse = await Database.mysqlDatabase.mysql.query(UserQueries.signinUserById, [Schema.USERS_TABLE, userId]);
			if (mysqlQueryResponse.rowCount === 1) {
				return mysqlQueryResponse.rows[0];
			}
		}
		catch (error) {
			logger.error('UserTools.getUserByEmail : Error retrieving user : %s', error.message);
		}
		return undefined;
	}

	/**
	 * Lookup the user in the database using the provided email
	 * @method getUserByEmail
	 * @public
	 * @static
	 * @async
	 * @param {string} email The users email to look up
	 * @returns {Promise<IUser | undefined>} returns the user or if lookup failed undefined.
	 */
	public static getUserByEmail = async (email: string): Promise<IUser | undefined> => {
		try {
			const mysqlQueryResponse = await Database.mysqlDatabase.mysql.query(UserQueries.signinUser, [Schema.USERS_TABLE, email]);
			if (mysqlQueryResponse.rowCount === 1) {
				return mysqlQueryResponse.rows[0];
			}
		}
		catch (error) {
			logger.error('UserTools.getUserByEmail : Error retrieving user : %s', error.message);
		}
		return undefined;
	}

	/**
	 * Reset the password for the selected account
	 * @method resetPassword
	 * @public
	 * @static
	 * @async
	 * @param {IPasswordResetRequest} resetPasswordRequest The password reset request
	 * @returns {Promise<IAccountResponse>} returns the account update response.
	 */
	public static resetPassword = async(resetPasswordRequest: IPasswordResetRequest): Promise<IAccountResponse> => {
		const user = await UserTools.getUserByEmail(resetPasswordRequest.email);
		if (user.user_forgotten_password_code === resetPasswordRequest.confirmationCode) {
			const resetTime: moment.Moment = moment(user.user_forgotten_password_time);
			if (moment.duration(moment().diff(resetTime)).asMinutes() > 60) {
				return {result: false, message: 'Confirmation code expired'};
			} else {
				return UserTools.updatePassword(user.user_id, resetPasswordRequest.password);
			}
		} else {
			return {result: false, message: 'Invalid confirmation code'};
		}
	}

	/**
	 * Reset the password for the selected account
	 * @method verifyEmailAddress
	 * @public
	 * @static
	 * @async
	 * @param {string} email The email address to confirm
	 * @param {string} confirmationCode The confirmation code to verify
	 * @returns {Promise<IAccountResponse>} returns the account update response.
	 */
	public static verifyEmailAddress = async(email: string, confirmationCode: string): Promise<void> => {
		const user = await UserTools.getUserByEmail(email);
		if (user && user.user_forgotten_password_code === confirmationCode) {
			user.user_verified = true;
			const mysqlQueryResponse: IMysqlResponse = await Database.mysqlDatabase.mysql.updateQuery(UserQueries.updateUserVerification, [Schema.USERS_TABLE, true, user.user_id]);
			console.log(mysqlQueryResponse);
		} else {
			logger.debug('UserTools.verifyEmailAddress : Confirmation code is invalid');
			return;
		}
	}

	/**
	 * Lookup the user in the database using the provided email
	 * @method updatePassword
	 * @public
	 * @static
	 * @async
	 * @param {IPasswordUpdate} passwordUpdate The update password request
	 * @returns {Promise<IAccountResponse>} returns the account update response.
	 */
	public static updatePassword = async (userId: number, passwordUpdate: string): Promise<IAccountResponse> => {
		const accountResponse: IAccountResponse = {
			message: '',
			result: false
		};
		try {
			const password: string = Password.hash(passwordUpdate);
			const mysqlQueryResponse: IMysqlResponse = await Database.mysqlDatabase.mysql.updateQuery(UserQueries.updatePassword, [Schema.USERS_TABLE, password, userId]);
			logger.debug('UserTools.updatePassword : Password change response : %o', mysqlQueryResponse);
			accountResponse.result = (mysqlQueryResponse.affectedRows === 1);
			accountResponse.message = (mysqlQueryResponse.affectedRows === 1) ? 'OK' : 'Password update failed';
		}
		catch (error) {
			logger.error('UserTools.updatePassword : Error updating user password : %s', error.message);
		}
		return accountResponse;
	}

	/**
	 * Lookup the user in the database using the provided email
	 * @method updatePassword
	 * @public
	 * @static
	 * @async
	 * @param {IUser} user The user to insert
	 * @returns {Promise<IAccountResponse>} returns the account update response.
	 */
	public static insertUser = async (user: IUser): Promise<IAccountResponse> => {
		const accountResponse: IAccountResponse = {
			message: '',
			result: false
		};
		try {
			user.user_forgotten_password_code = UserPasswordReset.generateResetCode();
			user.user_forgotten_password_time = moment().toDate();
			const mysqlQueryResponse: IMysqlResponse = await Database.mysqlDatabase.mysql.insertQuery(UserQueries.insertUser, [Schema.USERS_TABLE, user]);
			logger.debug('UserTools.insertUser : insert user response : %o', mysqlQueryResponse);
			if (mysqlQueryResponse.affectedRows === 1) {
				accountResponse.result = true;
				accountResponse.message = 'OK';
				const template: HandlebarsTemplateDelegate<any> = await EmailUtils.prepareEmailTemplate('emails/registration_template.html');
				UserTools.sendRegistrationEmail(user, template(user));
			} else {
				accountResponse.result = false;
				accountResponse.message = 'User Insert Failed';
			}
		}
		catch (error) {
			accountResponse.message = 'User insert failed';
			logger.error('UserTools.insertUser : Error updating user password : %s', error.message);
		}
		return accountResponse;
	}

	/**
	 * Lookup the user in the database using the provided email
	 * @method registerUser
	 * @public
	 * @static
	 * @param {IRegistrationRequest} registrationRequest The registration request
	 * @returns {IUser>} returns the user interface.
	 */
	public static registerUser = (registrationRequest: IRegistrationRequest): IUser => {
		return {
				user_id : undefined,
				user_email : registrationRequest.registration_email,
				user_suspended : false,
				user_firstname : registrationRequest.registration_firstname,
				user_surname : registrationRequest.registration_surname,
				user_password : Password.hash(registrationRequest.registration_password),
				user_password_expires_on : moment().add(5, 'years').toDate(),
				user_created_on : moment().toDate(),
				user_locked_out : false,
				user_last_login : undefined,
				user_failed_logins : 0,
				user_forgotten_password_code : undefined,
				user_forgotten_password_time : undefined,
				user_verified : false,
				user_image_filename : undefined
		};
	}

	/**
	 * Send the recovery email
	 * @method sendRegistrationEmail
	 * @private
	 * @static
	 * @param {IUser} user The user to send the email to
	 * @param {string} template The template to parse the user object into
	 * @returns {Promise<void>} returns an empty promise
	 */
	private static sendRegistrationEmail = (user: IUser, template: string): Promise<void> => {
		const mail = ReminderEmailer.createReminderEmail(user.user_email, 'Reminder: Registration', template);
		// console.log(template.toString());
		ReminderEmailer.sendReminderEmail(mail)
		.then((response: SMTPTransport.SentMessageInfo) => {
			console.log(response);
		});
		return;
	}
}