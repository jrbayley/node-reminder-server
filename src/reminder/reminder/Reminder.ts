const entities = require('html-entities').AllHtmlEntities;

import moment from 'moment';

import { TimeUtils } from '../../core/utils/TimeUtils';
import { IInsertReminder } from '../../restControllers/restRequests/IInsertReminder';
import { IReminder } from './IReminder';
import { ReminderValidResponse } from './ReminderValidResponse';

export class Reminder {
	public reminder_id : number;
	private user_id : number;
	private reminder_date : Date;
	private description : string;
	private period : number;
	private notes : string;
	private parent_id : number;
	private reminder_enabled : boolean;
	private reminder_archived : boolean;
	private reminder_time : string;
	private reminder_deleted : boolean;

	/**
	 * @constructor
	 * @param {IReminder} user The IUser interface to initlialise with
	 */

	constructor(reminder: IReminder) {
		this.reminder_id = reminder.reminder_id;
		this.user_id = reminder.user_id;
		this.reminder_date = moment(reminder.reminder_date).toDate();
		this.description = entities.encode(reminder.description);
		this.period = reminder.period;
		this.notes = entities.encode(reminder.notes);
		this.parent_id = reminder.parent_id;
		this.reminder_enabled = reminder.reminder_enabled;
		this.reminder_archived = reminder.reminder_archived;
		this.reminder_time = reminder.reminder_time;
		this.reminder_deleted = reminder.reminder_deleted;
	}

	/**
	 * @method
	 * Reminder is valid
	 * Checks the reminder validity and returns a response with message and result
	 * @returns {ReminderValidResponse} returns an IReminder interface from a reminder
	 */
	public isValidForInsert = (): ReminderValidResponse => {
		const responses: ReminderValidResponse[] = [];
		responses.push(this.reminderDateSet());
		responses.push(this.reminderDescriptionSet());
		responses.push(this.reminderUserSet());
		responses.push(this.reminderTimeSet());
		responses.push(this.reminderDescriptionValid());
		responses.push(this.reminderNotesValid());
		for (const validation of responses) {
			if (!validation.success) {
				return validation;
			}
		}
		return {success: true, message: 'ok'};
	}

	/**
	 * @method
	 * Reminder is valid
	 * Checks the reminder validity and returns a response with message and result
	 * @returns {ReminderValidResponse} returns an IReminder interface from a reminder
	 */
	public isValid = (): ReminderValidResponse => {
		const responses: ReminderValidResponse[] = [];
		responses.push(this.reminderIdSet());
		responses.push(this.isValidForInsert());
		for (const validation of responses) {
			if (!validation.success) {
				return validation;
			}
		}
		return {success: true, message: 'ok'};
	}

	/**
	 * @method
	 * Reminder ID is valid
	 * Checks the reminder validity and returns a response with message and result
	 * @returns {ReminderValidResponse} returns an IReminder interface from a reminder
	 */
	private reminderIdSet = (): ReminderValidResponse => {
		const valid: boolean = (this.reminder_id !== undefined);
		const message: string = (valid) ? 'valid' : 'No Reminder Id';
		return {success: valid, message: message};
	}

	/**
	 * @method
	 * Reminder date is valid
	 * Checks the reminder validity and returns a response with message and result
	 * @returns {ReminderValidResponse} returns an IReminder interface from a reminder
	 */
	private reminderDateSet = (): ReminderValidResponse => {
		const valid: boolean = (this.reminder_date !== undefined);
		const message: string = (valid) ? 'valid' : 'No Reminder Date';
		return {success: valid, message: message};
	}

	/**
	 * @method
	 * Reminder User is valid
	 * Checks the reminder validity and returns a response with message and result
	 * @returns {ReminderValidResponse} returns an IReminder interface from a reminder
	 */
	private reminderUserSet = (): ReminderValidResponse => {
		const valid: boolean = (this.user_id !== undefined);
		const message: string = (valid) ? 'valid' : 'No Reminder User';
		return {success: valid, message: message};
	}

	/**
	 * @method
	 * Reminder time is valid
	 * Checks the reminder validity and returns a response with message and result
	 * @returns {ReminderValidResponse} returns an IReminder interface from a reminder
	 */
	private reminderTimeSet = (): ReminderValidResponse => {
		const valid: boolean = this.reminder_time === undefined || this.reminder_time === '' || TimeUtils.timeStringIsValid(this.reminder_time);
		const message: string = (valid) ? 'valid' : 'Invalid Reminder Time';
		return {success: valid, message: message};
	}

	/**
	 * @method
	 * Reminder description is valid
	 * Checks the reminder validity and returns a response with message and result
	 * @returns {ReminderValidResponse} returns an IReminder interface from a reminder
	 */
	private reminderDescriptionSet = (): ReminderValidResponse => {
		const valid: boolean = (this.description !== undefined && this.description !== '');
		const message: string = (valid) ? 'valid' : 'No Reminder Description';
		return {success: valid, message: message};
	}

	/**
	 * @method
	 * Reminder description is valid
	 * Checks the reminder validity and returns a response with message and result
	 * @returns {ReminderValidResponse} returns an IReminder interface from a reminder
	 */
	private reminderDescriptionValid = (): ReminderValidResponse => {
		const valid: boolean = (this.description && this.description.length < 64);
		const message: string = (valid) ? 'valid' : 'Reminder Description too long';
		return {success: valid, message: message};
	}

	/**
	 * @method
	 * Reminder description is valid
	 * Checks the reminder validity and returns a response with message and result
	 * @returns {ReminderValidResponse} returns an IReminder interface from a reminder
	 */
	private reminderNotesValid = (): ReminderValidResponse => {
		const valid: boolean = (this.notes === undefined || this.notes === '' || (this.notes && this.notes.length > 2048));
		const message: string = (valid) ? 'valid' : 'Reminder Notes too long';
		return {success: valid, message: message};
	}

	/**
	 * @method
	 * @static
	 * Reminder interface generator
	 * Used to generate the response that contains reminder data
	 * @returns {IReminder} returns an IReminder interface from a reminder
	 */
	public static reminderAsJson = (reminder: Reminder): IReminder => {
		return {
			reminder_id: reminder.reminder_id,
			user_id: reminder.user_id,
			reminder_date: reminder.reminder_date,
			description: reminder.description,
			period: reminder.period,
			notes: reminder.notes,
			parent_id: reminder.parent_id,
			reminder_enabled: reminder.reminder_enabled,
			reminder_archived: reminder.reminder_archived,
			reminder_time: reminder.reminder_time,
			reminder_deleted: reminder.reminder_deleted
		};
	}


	/**
	 * @method
	 * @static
	 * Reminder interface generator
	 * Used to generate the response that contains reminder data
	 * @returns {IReminder} returns an IReminder interface from a reminder
	 */
	public static reminderInterfaceFromInsertData = (reminder: IInsertReminder): IReminder => {
		return {
			reminder_id: undefined,
			user_id: reminder.user_id,
			reminder_date: moment(reminder.reminder_date).toDate(),
			description: reminder.reminder_description,
			period: reminder.reminder_period,
			notes: reminder.reminder_notes,
			parent_id: undefined,
			reminder_enabled: true,
			reminder_archived: false,
			reminder_time: reminder.reminder_time,
			reminder_deleted: false
		};
	}
}