import _, { ListIterateeCustom } from 'lodash';
import moment from 'moment';

import { Database } from '../../core/database/Database';
import { IMysqlResponse } from '../../core/database/mysql/models/IMysqlResponse';
import logger from '../../core/logger/Log';
import { IReminderUpdate } from '../../restControllers/restRequests/IReminderUpdate';
import { Schema } from '../Schema';
import { IReminder } from './IReminder';
import { IReminderPlan } from './IReminderPlan';
import { Reminder } from './Reminder';
import { ReminderInsertResponse } from './ReminderInsertResponse';
import { ReminderPredicate } from './ReminderPredicate';
import { ReminderQueries } from './ReminderQueries';

/**
 * @class
 * Tools for retrieving and managing reminder objects
 */
export class ReminderTools {

	/**
	 * insert a new reminder into the database
	 * @method addReminder
	 * @static
	 * @param {Reminder} reminder The reminder to insert
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of reminder data
	 */
	public static addReminder = async (reminder: Reminder): Promise<ReminderInsertResponse> => {
		const valid = reminder.isValidForInsert();
		const response: ReminderInsertResponse = (<ReminderInsertResponse>valid);
		const isNotDuplicate: boolean = await ReminderTools.reminderIsNotDuplicate(Reminder.reminderAsJson(reminder));
		logger.debug('ReminderTools.addReminder : Checking for duplicate');
		if (response.success && isNotDuplicate) {
			const mysqlResponse: IMysqlResponse = await ReminderTools.insertReminder(Reminder.reminderAsJson(reminder));
			if (mysqlResponse.affectedRows === 1) {
				response.success = true;
				response.message = 'Update Count : ' + mysqlResponse.affectedRows;
				response.reminder = await ReminderTools.getReminder((<number>mysqlResponse.insertId));
			}
			return response;
		} else {
			logger.debug('ReminderTools.addReminder : Duplicate reminder');
			response.success = false;
			response.message = 'Duplicate reminder';
			response.reminder = Reminder.reminderAsJson(reminder);
			return response;
		}
	}

	/**
	 * getUserReminder get a specific reminder for a user
	 * @method saveReminder
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of reminder data
	 */
	public static saveReminder = async (reminderUpdate: IReminderUpdate): Promise<IReminder> => {
		for (const key of Object.keys(reminderUpdate)) {
			if (key !== 'reminder_id' && key !== 'user_id') {
				if (key === 'reminder_date') {
					logger.debug('ReminderTools.saveReminder : Update date to %s', moment((<any>reminderUpdate)[key]).toDate());
					await ReminderTools.updateReminderDate(key, moment((<any>reminderUpdate)[key]).toDate(), reminderUpdate.reminder_id, reminderUpdate.user_id);
				} else {
					await ReminderTools.updateReminder(key, (<any>reminderUpdate)[key], reminderUpdate.reminder_id, reminderUpdate.user_id);
					logger.debug('ReminderTools.saveReminder : Update %s to %s', key, (<any>reminderUpdate)[key]);
				}
			}
		  }
		return ReminderTools.getUserReminder(reminderUpdate.user_id, reminderUpdate.reminder_id);
	}


	/**
	 * getUserReminder get a specific reminder for a user
	 * @method getUserReminders
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of reminder data
	 */
	public static getUserReminders = async (userId: number): Promise<IReminder[]> => {
		return ReminderTools.selectUserReminders(userId);
	}

	/**
	 * getUserReminder get a specific reminder for a user
	 * @method getUserReminder
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @param {number} reminderId The reminder to look up
	 * @returns {Promise<IReminder | undefined>} returns a promise containing the reminder data or undefined if there is no match
	 */
	public static getUserReminder = async (userId: number, reminderId: number): Promise<IReminder | undefined> => {
		try {
			const mysqlQueryResponse = await Database.mysqlDatabase.mysql.query(ReminderQueries.getUserReminder, [Schema.REMINDERS_TABLE, userId, reminderId]);
			if (mysqlQueryResponse.rowCount === 1) {
				return mysqlQueryResponse.rows[0];
			}
		}
		catch (error) {
			logger.error('ReminderTools.getUserReminders : Error retrieving user reminders : %s', error.message);
		}
		return undefined;
	}

	/**
	 * getReminder get a specific reminder
	 * @method getReminder
	 * @private
	 * @static
	 * @param {number} reminderId The reminder to look up
	 * @returns {Promise<IReminder | undefined>} returns a promise containing the reminder data or undefined if there is no match
	 */
	private static getReminder = async (reminderId: number): Promise<IReminder | undefined> => {
		try {
			const mysqlQueryResponse = await Database.mysqlDatabase.mysql.query(ReminderQueries.getReminder, [Schema.REMINDERS_TABLE, reminderId]);
			if (mysqlQueryResponse.rowCount === 1) {
				return mysqlQueryResponse.rows[0];
			}
		}
		catch (error) {
			logger.error('ReminderTools.getReminders : Error retrieving reminder : %s', error.message);
		}
		return undefined;
	}

	/**
	 * Check to see if there is a matching reminder already in the database
	 * @method checkForDuplicateReminder
	 * @private
	 * @static
	 * @param {IReminder} reminder The reminder to check
	 * @returns {Promise<boolean>} returns a promise containing true if there are duplicates
	 */
	public static reminderIsNotDuplicate = async(reminder: IReminder): Promise<boolean> => {
		let duplicates: IReminder[] = undefined;
		try {
			duplicates = await ReminderTools.duplicateReminderCheck(reminder);
			logger.debug('ReminderTools.checkForDuplicateReminder : Found %d existing reminders', duplicates.length);
			return ! (duplicates && duplicates.length > 0);
		} catch (error) {
			logger.error('ReminderTools.checkForDuplicateReminder : Error checking duplicates ; %s', error.message);
		}
		return true;
	}
	/**
	 * getReminder get a specific reminder
	 * @method duplicateReminderCheck
	 * @private
	 * @static
	 * @param {number} reminderId The reminder to look up
	 * @returns {Promise<IReminder | undefined>} returns a promise containing the reminder data or undefined if there is no match
	 */
	private static duplicateReminderCheck = async (reminder: IReminder): Promise<IReminder[] | undefined> => {
		try {
			const mysqlQueryResponse = await Database.mysqlDatabase.mysql.query(ReminderQueries.duplicateReminderCheck, [Schema.REMINDERS_TABLE, reminder.user_id, reminder.description, reminder.reminder_date]);
			return mysqlQueryResponse.rows;
		}
		catch (error) {
			logger.error('ReminderTools.duplicateReminderCheck : Error retrieving reminder : %s', error.message);
		}
		return undefined;
	}

	/**
	 * getUserReminder get a specific reminder for a user
	 * @method getUserReminderPlan
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminderPlan[]>} returns a promise containing the reminderplan data or undefined if there are no plans
	 */
	public static getUserReminderPlan = async (userId: number): Promise<IReminderPlan[]> => {
		try {
			const mysqlQueryResponse = await Database.mysqlDatabase.mysql.query(ReminderQueries.getReminderPlan, [userId, userId, userId, userId, userId]);
			return mysqlQueryResponse.rows;
		}
		catch (error) {
			logger.error('ReminderTools.getUserReminders : Error retrieving user reminders : %s', error.message);
		}
		return [];
	}

	/**
	 * getActiveUserReminders get all a users reminders
	 * @method getActiveUserReminders
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of active reminder data or undefined if there is no match
	 */
	public static getActiveUserReminders = async (userId: number): Promise<IReminder[]> => {
		const predicate: ReminderPredicate = {reminder_enabled: true, reminder_archived: false};
		return ReminderTools.filterReminders(userId, predicate);
	}

	/**
	 * getArchivedUserReminders get all a users reminders
	 * @method getArchivedUserReminders
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of archived reminder data or undefined if there is no match
	 */
	public static getArchivedUserReminders = async (userId: number): Promise<IReminder[]> => {
		const predicate: ReminderPredicate = {reminder_enabled: true, reminder_archived: true};
		return ReminderTools.filterReminders(userId, predicate);
	}

	/**
	 * getDisabledUserReminders get all a users reminders
	 * @method getDisabledUserReminders
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of archived reminder data or undefined if there is no match
	 */
	public static getDisabledUserReminders = async (userId: number): Promise<IReminder[]> => {
		const predicate: ReminderPredicate = {reminder_enabled: false};
		return ReminderTools.filterReminders(userId, predicate);
	}

	/**
	 * getEnabledUserReminders get all a users reminders
	 * @method getEnabledUserReminders
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of archived reminder data or undefined if there is no match
	 */
	public static getEnabledUserReminders = async (userId: number): Promise<IReminder[]> => {
		const predicate: ReminderPredicate = {reminder_enabled: true};
		return ReminderTools.filterReminders(userId, predicate);
	}

	/**
	 * getEnabledUserReminders get all a users reminders
	 * @method filterReminders
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of archived reminder data or undefined if there is no match
	 */
	private static filterReminders = async (userId: number, predicate: ListIterateeCustom<IReminder, boolean>): Promise<IReminder[]> => {
		const reminders: IReminder[] = await ReminderTools.getUserReminders(userId);
		return _.filter(reminders, predicate);
	}

	/**
	 * getUserReminders get all a users reminders
	 * @method selectUserReminders
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of reminder data or undefined if there is no match
	 */
	private static selectUserReminders = async (userId: number): Promise<IReminder[]> => {
		try {
			const mysqlQueryResponse = await Database.mysqlDatabase.mysql.query(ReminderQueries.getUserReminders, [Schema.REMINDERS_TABLE, userId]);
			if (mysqlQueryResponse.rowCount > 0) {
				return mysqlQueryResponse.rows;
			}
		}
		catch (error) {
			logger.error('ReminderTools.getUserReminders : Error retrieving user reminders : %s', error.message);
		}
		return [];
	}

	/**
	 * insert Reminder insert a new reminder
	 * @method insertReminder
	 * @private
	 * @static
	 * @param {IReminder} IReminder The user id to filter reminders on
	 * @returns {Promise<IMysqlResponse]>} returns a promise containing the insert response
	 */
	private static insertReminder = (reminder: IReminder): Promise<IMysqlResponse> => {
		return Database.mysqlDatabase.mysql.insertQuery(ReminderQueries.insertReminder, [Schema.REMINDERS_TABLE, reminder]);
	}

	/**
	 * updateReminder update a single reminder field
	 * @method updateReminder
	 * @public
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of reminder data or undefined if there is no match
	 */
	private static updateReminder = (reminderField: string, reminderValue: string | number | Date, reminderId: number, userId: number): Promise<IMysqlResponse> => {
		return Database.mysqlDatabase.mysql.updateQuery(ReminderQueries.updateReminder, [Schema.REMINDERS_TABLE, reminderField, reminderValue, reminderId, userId]);
	}

	/**
	 * updateReminder update a single reminder field
	 * @method updateReminderDate
	 * @private
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of reminder data or undefined if there is no match
	 */
	private static updateReminderDate = (reminderField: string, reminderValue: string | number | Date, reminderId: number, userId: number): Promise<IMysqlResponse> => {
		return Database.mysqlDatabase.mysql.updateQuery(ReminderQueries.updateReminderDate, [Schema.REMINDERS_TABLE, reminderField, reminderValue, reminderId, userId]);
	}

	/**
	 * deleteReminder delete a reminder by setting the delete flag
	 * @method deleteReminder
	 * @private
	 * @static
	 * @param {number} userId The user id to filter reminders on
	 * @returns {Promise<IReminder[]]>} returns a promise containing an array of reminder data or undefined if there is no match
	 */
	public static deleteReminder = (reminderId: number, userId: number): Promise<IMysqlResponse> => {
		console.log(ReminderQueries.deleteReminder)
		return Database.mysqlDatabase.mysql.updateQuery(ReminderQueries.deleteReminder, [Schema.REMINDERS_TABLE, reminderId, userId]);
	}
}