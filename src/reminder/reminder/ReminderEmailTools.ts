import { Database } from '../../core/database/Database';
import logger from '../../core/logger/Log';
import { IReminderToSend } from '../email/IReminderToSend';
import { Schema } from '../Schema';
import { ReminderQueries } from './ReminderQueries';

/**
 * @class
 * Tools for retrieving and managing reminder objects
 */
export class ReminderEmailTools {

	/**
	 * @method
	 * @static
	 * getUserReminder get a specific reminder for a user
	 * @param {number} period The period to lookup
	 * @returns {Promise<IReminderToSend[]]>} returns a promise containing an array of reminder data
	 */
	public static getRemindersToEmail = async (reminderPeriod: number): Promise<IReminderToSend[]> => {
		try {
			const mysqlQueryResponse = await Database.mysqlDatabase.mysql.query(ReminderQueries.getRemindersToSendEmails, [Schema.REMINDERS_TABLE, reminderPeriod, reminderPeriod]);
			return mysqlQueryResponse.rows;
		}
		catch (error) {
			logger.error('ReminderTools.getReminders : Error retrieving reminder : %s', error.message);
		}
		return undefined;
	}
}