export interface ReminderPredicate {
	reminder_enabled? : boolean;
	reminder_archived? : boolean;
}