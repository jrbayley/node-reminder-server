import moment from 'moment';

export interface IReminderPlan {
	user_id : number;
	reminder_period : string;
	reminder_id : number;
	reminder_notes : string;
	reminder_repeat_period : string;
	reminder_date : moment.Moment;
	description : string;
	sent_date : moment.Moment;
	type : number;
}