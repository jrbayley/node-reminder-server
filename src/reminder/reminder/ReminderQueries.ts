/**
 * @class
 * Utility class to hold sql used to access reminders.
 */
export class ReminderQueries {
	/**
	 * @method
	 * @static
	 * @returns {string} The SQL to return a whole table with bind placeholders to add table
	 */
	public static getReminderData = 'SELECT * FROM ??';

	/**
	 * @method
	 * @static
	 * @returns {string} The SQL to return a users single reminder with bind placeholders for the table name userId and reminderId
	 */
	public static getUserReminder = 'SELECT * FROM ?? WHERE user_id = ? AND reminder_id = ?';

	/**
	 * @method
	 * @static
	 * @returns {string} The SQL to see if the user has any matching reminders for this date and description to avoid duplicates.
	 */
	public static duplicateReminderCheck = 'SELECT * FROM ?? WHERE user_id = ? AND description = ? AND reminder_date = ?';

	/**
	 * @method
	 * @static
	 * @returns {string} The SQL to return a users single reminder with bind placeholders for the table name userId and reminderId
	 */
	public static getReminder = 'SELECT * FROM ?? WHERE reminder_id = ?';

	/**
	 * @method
	 * @static
	 * @returns {string} The SQL to return a users reminder array with bind placeholder for table name and userId
	 */
	public static getUserReminders = 'SELECT * FROM ?? WHERE user_id = ?';

	/**
	 * @method
	 * @static
	 * @returns {string} The SQL to insert a reminder with bind placeholder for the table name and reminder object
	 */
	public static insertReminder = 'INSERT INTO ?? SET ?';

	/**
	 * @method
	 * @static
	 * @returns {string} The SQL to update a reminder with bind placeholders for the table name reminder object and table name
	 */
	public static updateReminder = 'UPDATE ?? SET ?? = ? WHERE reminder_id = ? AND user_id = ?';
	public static updateReminderDate = 'UPDATE ?? SET ?? = ? WHERE reminder_id = ? AND user_id = ?';

	/**
	 * @method
	 * @static
	 * @returns {string} The SQL to delete a single reminder with bind placeholders for the table name and reminderId
	 */
	public static deleteReminder = 'UPDATE ?? SET reminder_deleted = 1 WHERE reminder_id = ? AND user_id = ?';

	// tslint:disable: quotemark
	public static getReminderPlan = "	SELECT * FROM ( ( " +
											" SELECT  " +
												" u.user_id AS user_id, " +
												" 'due' AS reminder_period, " +
												" r.reminder_id, " +
												" r.notes AS reminder_notes, " +
												" (SELECT reminderPeriodCode FROM periods WHERE period = r.period ) AS reminder_repeat_period, " +
												" r.reminder_date AS reminder_date, " +
												" r.description, " +
												" ( " +
												"	SELECT  sent_date " +
												"	FROM remindersent rs " +
												"	WHERE r.reminder_id = rs.reminder_id " +
												"	AND r.reminder_date = rs.reminder_date " +
												"	AND type = 0 " +
												" ) AS sent_date, " +
												" 0 as type " +
												" FROM reminder r  " +
												" INNER JOIN user u ON r.user_id = u.user_id " +
												" WHERE u.user_id = ? " +
												" AND r.reminder_date >= CURDATE()  " +
												" AND r.reminder_date < ( DATE_ADD( CURDATE(), INTERVAL 11 MONTH ) ) " +
											" ) " +
											" UNION ALL " +
											" ( " +
												" SELECT   " +
												" u.user_id AS user_id, " +
												" 'tomorrow' AS reminder_period, " +
												" r.reminder_id, " +
												" r.notes AS reminder_notes, " +
												" (SELECT reminderPeriodCode FROM periods WHERE period = r.period ) AS reminder_repeat_period, " +
												" DATE_SUB(r.reminder_date, INTERVAL 1 DAY) AS reminder_date, " +
												" r.description, " +
												" ( " +
												"	SELECT sent_date " +
												"	FROM remindersent rs " +
												"	WHERE r.reminder_id = rs.reminder_id " +
												"	AND r.reminder_date = rs.reminder_date " +
												"	AND type = 1 " +
												" ) AS sent_date, " +
												" 1 as type " +
												" FROM reminder r  " +
												" INNER JOIN user u ON r.user_id = u.user_id " +
												" WHERE u.user_id = ? " +
												" AND r.reminder_date >= CURDATE()  " +
												" AND r.reminder_date < ( DATE_ADD( CURDATE(), INTERVAL 11 MONTH ) ) " +
											" ) " +
											" UNION ALL " +
											" ( " +
												" SELECT  u.user_id AS user_id, " +
												" 'week' AS reminder_period, " +
												" r.reminder_id, " +
												" r.notes AS reminder_notes, " +
												" (SELECT reminderPeriodCode FROM periods WHERE period = r.period ) AS reminder_repeat_period, " +
												" DATE_SUB(r.reminder_date, INTERVAL 7 DAY) AS reminder_date, " +
												" r.description, " +
												" ( " +
												"	SELECT sent_date " +
												"	FROM remindersent rs " +
												"	WHERE r.reminder_id = rs.reminder_id " +
												"	AND r.reminder_date = rs.reminder_date " +
												"	AND type = 7 " +
												" ) AS sent_date, " +
												" 7 as type " +
												" FROM reminder r  " +
												" INNER JOIN user u ON r.user_id = u.user_id " +
												" WHERE u.user_id = ? " +
												" AND r.reminder_date >= CURDATE()  " +
												" AND r.reminder_date < ( DATE_ADD( CURDATE(), INTERVAL 11 MONTH ) ) " +
											" ) " +
											" UNION ALL " +
											" ( " +
												" SELECT  u.user_id AS user_id, " +
												" 'fortnight' AS reminder_period, " +
												" r.reminder_id, " +
												" r.notes AS reminder_notes, " +
												" (SELECT reminderPeriodCode FROM periods WHERE period = r.period ) AS reminder_repeat_period, " +
												" DATE_SUB(r.reminder_date, INTERVAL 14 DAY) AS reminder_date, " +
												" r.description, " +
												" ( " +
												"	SELECT sent_date " +
												"	FROM remindersent rs " +
												"	WHERE r.reminder_id = rs.reminder_id " +
												"	AND r.reminder_date = rs.reminder_date " +
												"	AND type = 14 " +
												" ) AS sent_date, " +
												" 14 as type " +
												" FROM reminder r  " +
												" INNER JOIN user u ON r.user_id = u.user_id " +
												" WHERE u.user_id = ? " +
												" AND r.reminder_date >= CURDATE()  " +
												" AND r.reminder_date < ( DATE_ADD( CURDATE(), INTERVAL 11 MONTH ) ) " +
											" ) " +
											" UNION ALL " +
											" ( " +
												" SELECT  u.user_id AS user_id, " +
												" 'month' AS reminder_period, " +
												" r.reminder_id, " +
												" r.notes AS reminder_notes, " +
												" (SELECT reminderPeriodCode FROM periods WHERE period = r.period ) AS reminder_repeat_period, " +
												" DATE_SUB( r.reminder_date, INTERVAL 28 DAY ) AS reminder_date, " +
												" r.description, " +
												" ( " +
												" SELECT sent_date " +
												" FROM remindersent rs " +
												" WHERE r.reminder_id = rs.reminder_id " +
												" AND r.reminder_date = rs.reminder_date " +
												" AND type = 28 " +
												" ) AS sent_date, " +
												" 28 as type " +
												" FROM reminder r  " +
												" INNER JOIN user u ON r.user_id = u.user_id " +
												" WHERE u.user_id = ? " +
												" AND r.reminder_date >= CURDATE()  " +
												" AND r.reminder_date < ( DATE_ADD( CURDATE(), INTERVAL 11 MONTH ) ) " +
												" ) " +
											" ) AS AA " +
										" ORDER BY reminder_date ASC";

	// tslint:disable: quotemark
	public static getRemindersToSendEmails = "SELECT user.user_id, reminder.description, reminder.notes, reminder.reminder_date, reminder.period, reminder.reminder_time, reminder.reminder_id, user.user_email, user.user_firstname, user.user_surname" +
		" FROM ?? " +
		" INNER JOIN user ON reminder.user_id = user.user_id " +
		" WHERE DATE_SUB(reminder.reminder_date, INTERVAL ? DAY) = CURDATE() " +
		" AND reminder.reminder_deleted = 0 " +
		" AND reminder_id NOT IN " +
		"	( 	SELECT remindersent.reminder_id  " +
		"		FROM remindersent " +
		"		WHERE reminder.reminder_id = remindersent.reminder_id  " +
		"		AND reminder.reminder_date = remindersent.reminder_date " +
		"		AND remindersent.type = ? " +
		"	) " +
		" ORDER BY reminder.reminder_date ASC";
}