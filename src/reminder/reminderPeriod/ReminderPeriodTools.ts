import { Database } from '../../core/database/Database';
import { MysqlQueryResponse } from '../../core/database/mysql/MysqlQueryResponse';
import logger from '../../core/logger/Log';
import { ReminderQueries } from '../reminder/ReminderQueries';
import { Schema } from '../Schema';
import { IReminderPeriod } from './IReminderPeriod';

/**
 * @class
 * Tools for retrieving and managing period objects
 */
export class ReminderPeriodTools {

	/**
	 * @method
	 * @static
	 * getReminderPeriods get all reminder periods
	 * @returns {Promise<IReminderPeriod[]>} returns a promise containing the reminderPeriod data
	 */
	public static getReminderPeriods = async (): Promise<IReminderPeriod[]> => {
		try {
			const mysqlQueryResponse: MysqlQueryResponse = await Database.mysqlDatabase.mysql.query(ReminderQueries.getReminderData, [Schema.REMINDER_PERIOD_TABLE]);
			return mysqlQueryResponse.rows;
		}
		catch (error) {
			logger.error('ReminderPeriodTools.getReminderPeriods : Error retrieving reminder periods : %s', error.message);
		}
		return [];
	}
}