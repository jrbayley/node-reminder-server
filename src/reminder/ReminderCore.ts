import { ReminderSendReminder } from './email/ReminderSendReminder';

export class ReminderCore {

	public static reminderSendReminder : ReminderSendReminder;

	public static init = (): void => {
		ReminderCore.reminderSendReminder = new ReminderSendReminder();
	}
}