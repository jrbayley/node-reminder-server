

export interface IReminderToSend {
	user_id : number;
	user_email : string;
	user_firstname : string;
	user_surname : string;
	description : string;
	notes : string;
	reminder_id : number;
	reminder_date : string;
	period : number;
	reminder_time : string;
	reminderPeriod : string;
	reminderDate : string;
	howLong : string;
	firstname : string;
	surname : string;
}