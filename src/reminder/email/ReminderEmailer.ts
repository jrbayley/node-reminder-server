import SMTPTransport, { MailOptions } from 'nodemailer/lib/smtp-transport';

import { Comms } from '../../core/comms/Comms';
import { Configuration } from '../../core/config/Configuration';
import { StringUtils } from '../../core/utils/StringUtils';

export class ReminderEmailer {

	/**
	 * Generate the email options to send a reminder email
	 * @method
	 * @public
	 * @static
	 * @param {string} to The email address to send to
	 * @param {string} subject The email subject
	 * @param {string} text The email body
	 * @returns {MailOptions} The email mailoptions for sending
	 */
	public static createReminderEmail = (to: string, subject: string, html: string): MailOptions => {
		if (StringUtils.isValidEmail(to)) {
			return {
				from: Configuration.EMAIL_FROM,
				to: to,
				subject: subject,
				html: html
			};
		} else {
			return;
		}
	}

	/**
	 * Send the reminder email returning the response. Fire the sent listener
	 * @method
	 * @public
	 * @static
	 * @param {MailOptions} mailOptions The email mailoptions for sending
	 */
	public static sendReminderEmail = (mailOptions: MailOptions): Promise<SMTPTransport.SentMessageInfo> => {
		return Comms.emailSender.sendEmail(mailOptions);
	}
}