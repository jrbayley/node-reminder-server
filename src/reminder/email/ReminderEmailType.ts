import { IReminderTypeDefinition } from './IReminderTypeDefinition';

export class ReminderEmailType {
	public static oneMonth 	= (): IReminderTypeDefinition => {
		return { 	template: 'emails/reminder_template_default.html',
					period: 30
				};
	};

	public static twoWeek	= (): IReminderTypeDefinition => {
		return { 	template: 'emails/reminder_template_default.html',
					period: 14
				};
	};

	public static oneWeek	= (): IReminderTypeDefinition => {
		return { 	template: 'emails/reminder_template_default.html',
					period: 7
				};
	};

	public static tomorrow 	= (): IReminderTypeDefinition => {
		return { 	template: 'emails/reminder_template_tomorrow.html',
					period: 1
				};
	};

	public static today		= (): IReminderTypeDefinition => {
		return { 	template: 'emails/reminder_template_today.html',
					period: 0
				};
	};

	public static expired	= (): IReminderTypeDefinition => {
		return { 	template: 'emails/reminder_template_expired.html',
					period: undefined
				};
	};

	public static renewed 	= (): IReminderTypeDefinition => {
		return { 	template: 'emails/reminder_template_renewed.html',
					period: undefined
				};
	};

	public static added		= (): IReminderTypeDefinition => {
		return { 	template: 'emails/reminder_template_acknowledge.html',
					period: undefined
				};
	};

}