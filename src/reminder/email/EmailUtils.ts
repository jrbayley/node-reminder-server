import Handlebars from 'handlebars';

import { TemplateUtils } from '../../core/utils/TemplateUtils';

export class EmailUtils {


	public static prepareEmailTemplate = async(templateFile: string): Promise<HandlebarsTemplateDelegate> => {
		const html = await TemplateUtils.readFile(templateFile);
		return Handlebars.compile(html);
	}
}