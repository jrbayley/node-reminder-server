import _ from 'lodash';
import moment from 'moment';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

import { Listeners } from '../../core/listener/Listeners';
import logger from '../../core/logger/Log';
import { TimeUtils } from '../../core/utils/TimeUtils';
import { ReminderEmailer } from '../email/ReminderEmailer';
import { ReminderEmailTools } from '../reminder/ReminderEmailTools';
import { IReminderPeriod } from '../reminderPeriod/IReminderPeriod';
import { ReminderPeriodTools } from '../reminderPeriod/ReminderPeriodTools';
import { EmailUtils } from './EmailUtils';
import { IReminderToSend } from './IReminderToSend';
import { IReminderTypeDefinition } from './IReminderTypeDefinition';
import { ReminderEmailType } from './ReminderEmailType';

export class ReminderSendReminder {

	private reminderPeriods : IReminderPeriod[];

	constructor () {
		ReminderPeriodTools.getReminderPeriods()
		.then((reminderPeriods: IReminderPeriod[]) => {
			this.reminderPeriods = reminderPeriods;
		})
		.catch((error: Error) => {
			logger.error('ReminderSendReminder : Unable to get reminder periods : %s', error.message);
			this.reminderPeriods = [];
		});
		Listeners.dailySchedule.subscribe(this.checkForWeekEmails);
	}

	private checkForWeekEmails = (): Promise<void> => {

		return this.checkForEmailsToSend(ReminderEmailType.oneWeek());
	}


	private checkForEmailsToSend = async(emailType: IReminderTypeDefinition): Promise<void> => {
 		const template = await EmailUtils.prepareEmailTemplate(emailType.template);
		const remindersToSend: IReminderToSend[] = await ReminderEmailTools.getRemindersToEmail(emailType.period);
		for (let reminderToSend of remindersToSend) {
			reminderToSend = this.prepareInterfaceForTemplate(reminderToSend);
			logger.debug('ReminderSendReminder.sendDailyReminders : %s', reminderToSend);
			const processedTemplate: string = template(reminderToSend);
			this.sendReminderEmail(reminderToSend, processedTemplate);
		}
		return;
	}


	private sendReminderEmail = (reminderToSend: IReminderToSend, template: string): Promise<void> => {
		const mail = ReminderEmailer.createReminderEmail(reminderToSend.user_email, 'Reminder: ' + reminderToSend.description, template);
		// console.log(template.toString());
		ReminderEmailer.sendReminderEmail(mail)
		.then((response: SMTPTransport.SentMessageInfo) => {
			console.log(response);
		});
		return;
	}


	private prepareInterfaceForTemplate = (reminderToSend: IReminderToSend): IReminderToSend => {
		const reminderPeriod = _.find(this.reminderPeriods, {period: reminderToSend.period});
		reminderToSend.reminderPeriod = reminderPeriod.period_desc;
		reminderToSend.firstname = reminderToSend.user_firstname;
		reminderToSend.surname = reminderToSend.user_surname;
		reminderToSend.reminderDate = TimeUtils.momentAsHumanDate(moment(reminderToSend.reminder_date));
		reminderToSend.howLong = TimeUtils.calculateHowLong(moment(reminderToSend.reminder_date));
		return reminderToSend;
	}
}