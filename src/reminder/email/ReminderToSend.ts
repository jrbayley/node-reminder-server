const entities = require('html-entities').AllHtmlEntities;

import { IReminderToSend } from './IReminderToSend';

export class ReminderToSend implements IReminderToSend{
	public user_id : number;
	public user_email : string;
	public user_firstname : string;
	public user_surname : string;
	public description : string;
	public notes : string;
	public reminder_id : number;
	public reminder_date : string;
	public period : number;
	public reminder_time : string;
	public reminderPeriod : string;
	public reminderDate : string;
	public howLong : string;
	public firstname : string;
	public surname : string;

	/**
	 * @constructor
	 * @param {IReminderToSend} reminderToSend The IReminderToSend interface to initlialise with
	 */

	constructor(reminderToSend: IReminderToSend) {
		this.reminder_id = reminderToSend.reminder_id;
		this.user_id = reminderToSend.user_id;
		this.reminder_date = reminderToSend.reminder_date;
		this.description = entities.encode(reminderToSend.description);
		this.period = reminderToSend.period;
		this.notes = entities.encode(reminderToSend.notes);
		this.reminder_time = reminderToSend.reminder_time;
}