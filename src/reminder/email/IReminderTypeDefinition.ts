export interface IReminderTypeDefinition {
	template : string;
	period : number;
}