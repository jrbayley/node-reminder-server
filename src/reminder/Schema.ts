/**
 * @class
 * The schema configuration
 */

export class Schema {

	/** @var {string} USERS_TABLE The table for users */
	public static USERS_TABLE 				= 'user';

	/** @var {string} REMINDERS_TABLE The table for users */
	public static REMINDERS_TABLE 			= 'reminder';

	/** @var {string} REMINDER_PERIOD_TABLE The table for users */
	public static REMINDER_PERIOD_TABLE 	= 'periods';
}