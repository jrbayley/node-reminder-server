import * as express from 'express';

import { Configuration } from '../core/config/Configuration';
import startupLogger from '../core/logger/StartupLog';
import { IReminderPeriod } from '../reminder/reminderPeriod/IReminderPeriod';
import { ReminderPeriodTools } from '../reminder/reminderPeriod/ReminderPeriodTools';
import { IRestController } from './IRestController';

/**
 *
 * Rest controls for reminder periods.
 * @class
 *
 */
export class ReminderPeriodController implements IRestController {

	/** @var {string} path  The base url for this controller */
	public path = Configuration.BASE_URI + '/';

	/** @var {express.Router} router  The express router reference */
	public router : express.Router = express.Router();

	/**
	 * @constructor
	 */
	constructor() {
		this.intializeRoutes();
	}

	/**
	 * @method
	 * Initialise the routes for this controller.
	 */
	public intializeRoutes() {
		this.router.get(this.path + 'reminderPeriods', this.reminderPeriods);
		startupLogger.startup('Configured GET endpoint ' + this.path + 'reminderPeriods');
	}

	/**
	 * @method
	 * @private
	 * Return the reminder periods.
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private reminderPeriods = async (req: express.Request, res: express.Response): Promise<void> => {
		if ((<any>req).user && (<any>req).user.id) {
			const reminderPeriods: IReminderPeriod[] = await ReminderPeriodTools.getReminderPeriods();
			res.send(reminderPeriods);
		} else {
			res.status(404);
			res.send();
		}
	}
}
