export interface IAccountResponse {

	/** @var {string} message The response from updating the account */
	message? : string;

	/** @var {boolean} result The result success / fail */
	result? : boolean;
}