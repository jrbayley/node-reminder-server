export class IRegistrationRequest {

	/** @var  {string} registration_firstname Registration firstname */
	registration_firstname : string;

	/** @var  {string} registration_surname Registration surname */
	registration_surname : string;

	/** @var  {string} registration_email Registration email */
	registration_email : string;

	/** @var  {string} registration_password Registration password */
	registration_password : string;

	/** @var  {string} registration_password_confirm Registration password confirmation */
	registration_password_confirm : string;

}