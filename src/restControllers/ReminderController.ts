import * as express from 'express';

import { Configuration } from '../core/config/Configuration';
import { IMysqlResponse } from '../core/database/mysql/models/IMysqlResponse';
import logger from '../core/logger/Log';
import startupLogger from '../core/logger/StartupLog';
import { IReminder } from '../reminder/reminder/IReminder';
import { Reminder } from '../reminder/reminder/Reminder';
import { ReminderInsertResponse } from '../reminder/reminder/ReminderInsertResponse';
import { ReminderTools } from '../reminder/reminder/ReminderTools';
import { IRestController } from './IRestController';
import { IInsertReminder } from './restRequests/IInsertReminder';
import { IReminderUpdate } from './restRequests/IReminderUpdate';

/**
 *
 * Reminders rest controller for user reminders.
 * @class
 *
 */
export class ReminderController implements IRestController {

	/** @var {string} path  The base url for this controller */
	public path = Configuration.BASE_URI + '/';

	/** @var {express.Router} router  The express router reference */
	public router : express.Router = express.Router();

	/**
	 * @constructor
	 */
	constructor() {
		this.intializeRoutes();
	}

	/**
	 * Initialise the routes for this controller.
	 * @method intializeRoutes
	 * @public
	 */
	public intializeRoutes() {
		this.router.get(this.path + 'reminder/:id/', this.reminder);
		startupLogger.startup('Configured GET endpoint ' + this.path + 'reminder/:id/');

		this.router.get(this.path + 'reminders', this.reminders);
		startupLogger.startup('Configured GET endpoint ' + this.path + 'reminders');

		this.router.post(this.path + 'reminder', this.postReminder);
		startupLogger.startup('Configured POST endpoint ' + this.path + 'reminder');

		this.router.put(this.path + 'reminder', this.putReminder);
		startupLogger.startup('Configured PUT endpoint ' + this.path + 'reminder');

		this.router.delete(this.path + 'reminder/:id/', this.deleteReminder);
		startupLogger.startup('Configured DELETE endpoint ' + this.path + 'reminder');
	}

	/**
	 * Return the reminders that belong to a user.
	 * @method reminder
	 * @private
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private reminder = async (req: express.Request, res: express.Response): Promise<void> => {
		const reminderId: number = parseInt(req.params.id);
		console.log(reminderId);
		if ((<any>req).user && (<any>req).user.id) {
			const reminder: IReminder = await ReminderTools.getUserReminder((<any>req).user.id, reminderId);
			res.send(reminder);
		} else {
			res.status(404);
			res.send();
		}
	}

	/**
	 * Return the reminders that belong to a user.
	 * @method reminderEmail
	 * @private
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private reminderEmail = async (req: express.Request, res: express.Response): Promise<void> => {
		const reminderId: number = parseInt(req.params.id);
		console.log(reminderId);
		if ((<any>req).user && (<any>req).user.id) {
			const reminder: IReminder = await ReminderTools.getUserReminder((<any>req).user.id, reminderId);
			res.send(reminder);
		} else {
			res.status(404);
			res.send();
		}
	}

	/**
	 * Return the reminders that belong to a user.
	 * @method reminders
	 * @private
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private reminders = async (req: express.Request, res: express.Response): Promise<void> => {
		if ((<any>req).user && (<any>req).user.id) {
			const reminders: IReminder[] = await ReminderTools.getUserReminders((<any>req).user.id);
			res.send(reminders);
		} else {
			res.status(404);
			res.send('Unknown user ' + (<any>req).user);
		}
	}

	/**
	 * Return the reminders that belong to a user.
	 * @method putReminder
	 * @private
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private putReminder = async (req: express.Request, res: express.Response): Promise<void> => {
		if ((<any>req).user && (<any>req).user.id) {
			const reminderUpdate: IReminderUpdate = req.body;
			logger.debug('ReminderController: Processing reminder update : %o', reminderUpdate);
			if (reminderUpdate && reminderUpdate.user_id && reminderUpdate.reminder_id) {
				if (reminderUpdate.user_id === parseInt((<any>req).user.id)) {
					const reminder: IReminder = await ReminderTools.saveReminder(reminderUpdate);
					res.send(reminderUpdate);
				} else {
					res.status(400);
					res.send('Unable to update reminders belonging to another user : ' + (<any>req).user.id + ' : ' + reminderUpdate.user_id);
				}
			} else {
				res.status(400);
				res.send('No date to process. Did you set Content-Type application/json');
			}
		} else {
			res.status(400);
			res.send();
		}
	}

	/**
	 * Insert a new reminder.
	 * @method postReminder
	 * @private
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private postReminder = async (req: express.Request, res: express.Response): Promise<void> => {
		if ((<any>req).user && (<any>req).user.id) {
			const reminderData: IInsertReminder = req.body;
			logger.debug('ReminderController: Processing reminder insert : %o', reminderData);
			if (reminderData && reminderData.reminder_description && reminderData.reminder_date) {
				reminderData.user_id = parseInt((<any>req).user.id);
				const reminder: Reminder = new Reminder(Reminder.reminderInterfaceFromInsertData(reminderData));
				const result: ReminderInsertResponse = await ReminderTools.addReminder(reminder);
				res.send(result);
			} else {
				res.status(400);
				res.send('No data to process. Did you set Content-Type application/json' + JSON.stringify(reminderData));
			}
		} else {
			res.status(400);
			res.send();
		}
	}

	/**
	 * @method
	 * @private
	 * Return the reminders that belong to a user.
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private deleteReminder = async (req: express.Request, res: express.Response): Promise<void> => {
		const reminderId: number = parseInt(req.params.id);
		if ((<any>req).user && (<any>req).user.id) {
			const result: IMysqlResponse = await ReminderTools.deleteReminder(reminderId, (<any>req).user.id);
			const response: any = {deleted: result.affectedRows};
			res.send(response);
		} else {
			res.status(400);
			res.send('No data to process. Did you set Content-Type application/json');
		}
	}
}
