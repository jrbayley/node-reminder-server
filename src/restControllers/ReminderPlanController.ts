import * as express from 'express';

import { Configuration } from '../core/config/Configuration';
import startupLogger from '../core/logger/StartupLog';
import { IReminderPlan } from '../reminder/reminder/IReminderPlan';
import { ReminderTools } from '../reminder/reminder/ReminderTools';
import { IRestController } from './IRestController';

/**
 *
 * Reminders rest controller for user reminder plans..
 * @class
 *
 */
export class ReminderPlanController implements IRestController {

	/** @var {string} path  The base url for this controller */
	public path = Configuration.BASE_URI + '/';

	/** @var {express.Router} router  The express router reference */
	public router : express.Router = express.Router();

	/**
	 * @constructor
	 */
	constructor() {
		this.intializeRoutes();
	}

	/**
	 * @method
	 * @public
	 * Initialise the routes for this controller.
	 */
	public intializeRoutes() {
		this.router.get(this.path + 'reminderPlan', this.reminderPlan);
		startupLogger.startup('Configured GET endpoint ' + this.path + 'reminderPlan');
	}

	/**
	 * @method
	 * @private
	 * Return the reminder plans that belong to a user.
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private reminderPlan = async (req: express.Request, res: express.Response): Promise<void> => {
		if ((<any>req).user && (<any>req).user.id) {
			const reminderPlan: IReminderPlan[] = await ReminderTools.getUserReminderPlan((<any>req).user.id);
			res.send(reminderPlan);
		} else {
			res.status(404);
			res.send();
		}
	}
}
