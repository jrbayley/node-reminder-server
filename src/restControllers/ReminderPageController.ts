import * as express from 'express';

import { Configuration } from '../core/config/Configuration';
import startupLogger from '../core/logger/StartupLog';
import { TemplateUtils } from '../core/utils/TemplateUtils';
import { IRestController } from './IRestController';

/**
 *
 * Reminder page retrieve and send.
 * @class
 *
 */
export class ReminderPageController implements IRestController {

	/** @var {string} path  The base url for this controller */
	public path = Configuration.BASE_URI + '/';

	/** @var {express.Router} router  The express router reference */
	public router : express.Router = express.Router();

	/**
	 * @constructor
	 */
	constructor() {
		this.intializeRoutes();
	}

	/**
	 * @method
	 * @public
	 * Initialise the routes for this controller.
	 */
	public intializeRoutes() {
		this.router.get(this.path + 'reminderpage/:page', this.reminderPage);
		startupLogger.startup('Configured GET endpoint ' + this.path + 'reminderPage/:page');
	}

	/**
	 * @method
	 * @private
	 * Return the reminders that belong to a user.
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private reminderPage = async (req: express.Request, res: express.Response): Promise<void> => {
		const page = req.params.page;
		TemplateUtils.readFile(page)
		.then((file: string) => {
			res.send(file);
		})
		.catch((error: Error) => {
			res.status(404);
			res.send(error);
		});
	}
}
