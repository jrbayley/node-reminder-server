import * as express from 'express';

import { AppData } from '../core/appData/AppData';
import { Configuration } from '../core/config/Configuration';
import logger from '../core/logger/Log';
import startupLogger from '../core/logger/StartupLog';
import { IUserValidationResponse } from '../reminder/user/IUserValidationResponse';
import { UserTools } from '../reminder/user/UserTools';
import { IRestController } from './IRestController';
import { IAccountResponse } from './restRequests/IAccountResponse';
import { IPasswordUpdate } from './restRequests/IPasswordUpdate';

/**
 *
 * Utilitise for connecting to the AWS Dynamo Database and retreiving data.
 * @class
 *
 */
export class AccountController implements IRestController {

	/** @var {string} path  The base url for this controller */
	public path = Configuration.BASE_URI + '/';

	/** @var {express.Router} router  The express router reference */
	public router : express.Router = express.Router();

	/**
	 * @constructor
	 */
	constructor() {
		this.intializeRoutes();
	}

	/**
	 * @method
	 * @public
	 * Initialise the routes for this controller.
	 */
	public intializeRoutes() {
		this.router.put(this.path + 'account', this.updatePassword);
		startupLogger.startup('Configured PUT endpoint ' + this.path + 'account');
	}

	/**
	 * @method
	 * @private
	 * Return a 200 OK status.
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private updatePassword = async (req: express.Request, res: express.Response): Promise<void> => {
		if ((<any>req).user && (<any>req).user.id) {
			const passwordUpdate: IPasswordUpdate = req.body;
			logger.debug('AccountController.updatePassword : Received request : %o', passwordUpdate);
			const signin: IUserValidationResponse = await AppData.users.getUserById((<any>req).user.id, passwordUpdate.currentPassword);
			logger.debug('AccountController.updatePassword : Received signin response : %o', signin);
			let response: IAccountResponse = {
				result: false,
				message: 'Invalid Password'
			};
			if (signin.success) {
				if (passwordUpdate && passwordUpdate.currentPassword && passwordUpdate.newPassword) {
					try {
						response = await (UserTools.updatePassword((<any>req).user.id, passwordUpdate.newPassword));
						res.send(response);
					} catch (error) {
						this.returnError(res);
					}
				} else {
					this.returnError(res);
				}
			} else {
				logger.error('Error updating : bad password %o', signin);
				response.message = signin.message;
				res.send(response);
			}
		} else {
			logger.error('Error updating : bad token data');
			this.returnError(res);
		}
	}

	/**
	 * @method
	 * @private
	 * Return a 400 Bad request status.
	 * @param {express.Response} res Express response data
	 */
	private returnError = (res: express.Response): void => {
		res.status(400);
		res.send('Invalid Data');
	}
}
