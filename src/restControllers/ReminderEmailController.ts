import * as express from 'express';

import { Configuration } from '../core/config/Configuration';
import { Listeners } from '../core/listener/Listeners';
import startupLogger from '../core/logger/StartupLog';
import { IRestController } from './IRestController';

/**
 *
 * Reminders rest controller for user reminder plans..
 * @class
 *
 */
export class ReminderEmailController implements IRestController {

	/** @var {string} path  The base url for this controller */
	public path = Configuration.BASE_URI + '/';

	/** @var {express.Router} router  The express router reference */
	public router : express.Router = express.Router();

	/**
	 * @constructor
	 */
	constructor() {
		this.intializeRoutes();
	}

	/**
	 * @method
	 * @public
	 * Initialise the routes for this controller.
	 */
	public intializeRoutes() {
		this.router.get(this.path + 'reminderEmail', this.reminderEmail);
		startupLogger.startup('Configured GET endpoint ' + this.path + 'reminderEmail');
	}

	/**
	 * @method
	 * @private
	 * Return the reminder plans that belong to a user.
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private reminderEmail = async (_req: express.Request, res: express.Response): Promise<void> => {
		Listeners.dailySchedule.publish();
		res.send('OK');
	}
}
