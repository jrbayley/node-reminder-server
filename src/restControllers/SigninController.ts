import * as express from 'express';

import { AppData } from '../core/appData/AppData';
import { Configuration } from '../core/config/Configuration';
import startupLogger from '../core/logger/StartupLog';
import { JwtTools } from '../core/security/JwtTools';
import { StringUtils } from '../core/utils/StringUtils';
import { IUser } from '../reminder/user/IUser';
import { IUserValidationResponse } from '../reminder/user/IUserValidationResponse';
import { UserPasswordReset } from '../reminder/user/UserPasswordReset';
import { UserTools } from '../reminder/user/UserTools';
import { IRestController } from './IRestController';
import { IAccountResponse } from './restRequests/IAccountResponse';
import { IPasswordResetRequest } from './restRequests/IPasswordResetRequest';
import { IRegistrationRequest } from './restRequests/IRegistrationRequest';
import { ISigninRequest } from './restRequests/ISigninRequest';

/**
 *
 * Sign in and security encryption and decryption methods.
 * @class
 *
 */
export class SigninController implements IRestController {

	/** @var {string} path  The base url for this controller */
	public path = Configuration.BASE_URI + '/';

	/** @var {express.Router} router  The express router reference */
	public router : express.Router = express.Router();

	/**
	 * @constructor
	 */
	constructor() {
		this.intializeRoutes();
	}

	/**
	 * Initialise the routes for this controller.
	 * @method intializeRoutes
	 * @public
	 */
	public intializeRoutes() {
		this.router.post(this.path + 'signin', this.signin);
		startupLogger.startup('Configured POST endpoint ' + this.path + 'signin');

		this.router.get(this.path + 'publicKey', this.getPublicKey);
		startupLogger.startup('Configured GET endpoint ' + this.path + 'publicKey');

		this.router.get(this.path + 'recover', this.recovery);
		startupLogger.startup('Configured GET endpoint ' + this.path + 'recover');

		this.router.post(this.path + 'reset', this.resetPassword);
		startupLogger.startup('Configured POST endpoint ' + this.path + 'reset');

		this.router.post(this.path + 'register', this.register);
		startupLogger.startup('Configured POST endpoint ' + this.path + 'register');

		this.router.get(this.path + 'emailAvailable', this.emailAvailable);
		startupLogger.startup('Configured GET endpoint ' + this.path + 'emailAvailable');
	}

	/**
	 * Return signed JWT if the user info is correct.
	 * @method signin
	 * @private
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private signin = async (req: express.Request, res: express.Response): Promise<void> => {
		const signinRequest: ISigninRequest = req.body;
		if (signinRequest && signinRequest.username && signinRequest.password) {
			if (signinRequest.confirmation_code !== undefined) {
				await UserTools.verifyEmailAddress(signinRequest.username, signinRequest.confirmation_code)
			}
			const signin: IUserValidationResponse = await AppData.users.getUserByEmail(signinRequest.username, signinRequest.password);
			res.send(signin);
			return;
		} else {
			res.status(400);
			res.send('Invalid user credentials.');
		}
	}

	/**
	 * Return the public key for JWT auth.
	 * @method getPublicKey
	 * @private
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 */
	private getPublicKey = (_req: express.Request, res: express.Response): void => {
		const publicKey = JwtTools.getPublicKey();
		res.send(publicKey);
	}

	/**
	 * Send the recovery email to the user.
	 * @method recovery
	 * @private
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 */
	private recovery = async(req: express.Request, res: express.Response): Promise<void> => {
		const email = req.query.email;
		UserPasswordReset.resetRequestFor(email);
		res.send('OK');
	}

	/**
	 * Allow the user to reset their password.
	 * @method resetPassword
	 * @private
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 */
	private resetPassword = (req: express.Request, res: express.Response): void => {
		const resetRequest: IPasswordResetRequest = req.body;
		if (resetRequest && StringUtils.isValidEmail(resetRequest.email) &&  resetRequest.confirmPasword && resetRequest.password && resetRequest.confirmationCode) {
			if (resetRequest.confirmPasword === resetRequest.password) {
				UserTools.resetPassword(resetRequest)
				.then((result: IAccountResponse) => {
					res.send(result);
				})
			} else {
				const response: IAccountResponse = {result: false, message: 'The passwords do not match'};
				res.send(response);
			}
		} else {
			res.status(400);
			res.send('No Data');
		}
	}

	/**
	 * Check to see if the email address is available.
	 * @method emailAvailable
	 * @private
	 * @async
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 */
	private emailAvailable = async(req: express.Request, res: express.Response): Promise<void> => {
		const email: string = req.query.email;
		const available: boolean = await this.isEmailAvailable(email);
		res.send(available);
	}

	/**
	 * Register the user.
	 * @method isEmailAvailable
	 * @private
	 * @async
	 * @param {string} email Email to check
	 * @returns {Promise<boolean>} true if the email is available
	 */
	private isEmailAvailable = async(email: string): Promise<boolean> => {
		const user: IUser = await UserTools.getUserByEmail(email.toLowerCase());
		return (user === undefined);
	}

	/**
	 * Register the user.
	 * @method register
	 * @private
	 * @param {express.Request} req Express request data
	 * @param {express.Response} res Express response data
	 */
	private register = async(req: express.Request, res: express.Response): Promise<void> => {
		const registrationRequest: IRegistrationRequest = req.body;
		if (registrationRequest.registration_password === registrationRequest.registration_password_confirm) {
			const user: IUser = UserTools.registerUser(registrationRequest);
			const response = await UserTools.insertUser(user);
			res.send(response);
		} else {
			res.status(400);
			res.send('Passwords do not match');
		}
	}
}
