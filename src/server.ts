
import { AppData } from './core/appData/AppData';
import { Comms } from './core/comms/Comms';
import { Database } from './core/database/Database';
import { Listeners } from './core/listener/Listeners';
import startLogger from './core/logger/StartupLog';
import { RestServer } from './core/RestServer';
import { JwtTools } from './core/security/JwtTools';
import { ReminderCore } from './reminder/ReminderCore';

/**
 *
 * Bootstrap the monitoring service.
 * @class
 *
 */
startLogger.startup('MonitoringServer: Rest Service Starting');

const init = async (): Promise<void> => {
	Listeners.init();
	JwtTools.init();
	Database.init();
	AppData.init();
	Comms.init();
	ReminderCore.init();
	RestServer.init();
	RestServer.server.initialiseScheduler();
	RestServer.listen();
};

init();