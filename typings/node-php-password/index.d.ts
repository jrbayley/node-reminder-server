
declare module 'node-php-password' {
    export function hash(password: string, passwordAlgorithm?: string, options?: options): string;
    export function needsRehash(hash: string, string?: passwordAlgorithm, options?: options): boolean;
    export function verify(password: string, hash: string): boolean;

    interface options {
        cost: number,
        salt?: string
     }

    enum passwordAlgorithm {
        PASSWORD_DEFAULT = 'PASSWORD_DEFAULT',
        PASSWORD_BCRYPT = 'PASSWORD_BCRYPT'
    }
}