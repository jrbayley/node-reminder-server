var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
var moment = require('moment');
var TimeUtils = require ('../server/core/utils/TimeUtils.js');



describe('Time Util Checks', () => {
	describe('Creates a moment from a date string', () => {
		it('should return a valid moment', () => {
			TimeUtils.TimeUtils.stringDateToMoment('31-01-2020').isValid().should.equal(true);
		});

		it('should return a valid moment', () => {
			TimeUtils.TimeUtils.stringDateToMoment('31/01/2020').isValid().should.equal(true);
		});

		it('should return a valid moment', () => {
			TimeUtils.TimeUtils.stringDateToMoment('31/JAN/2020').isValid().should.equal(true);
		});

		it('should return a valid moment', () => {
			TimeUtils.TimeUtils.stringDateToMoment('31/JANUARY/2020').isValid().should.equal(true);
		});

		it('should return a valid moment', () => {
			TimeUtils.TimeUtils.stringDateTimeToMoment('31/JANUARY/2020', '10:00').isValid().should.equal(true);
		});

		it('should return a valid moment', () => {
			TimeUtils.TimeUtils.stringDateAndTimeToMoment('31/JANUARY/2020 10:00').isValid().should.equal(true);
		});
	});

	describe('Returns date values', () => {
		it('should return an integer from the month name', () => {
			TimeUtils.TimeUtils.monthAsInt('January').should.equal(1);
		});
		it('should return an integer from the month name', () => {
			TimeUtils.TimeUtils.monthAsInt('Jan').should.equal(1);
		});
		it('should return an integer from the month name', () => {
			TimeUtils.TimeUtils.monthAsInt('December').should.equal(12);
		});
		it('should return an integer from the month name', () => {
			TimeUtils.TimeUtils.monthAsInt('Dec').should.equal(12);
		});

		it('should return an string from the month name with a leading zero', () => {
			TimeUtils.TimeUtils.monthNameAsNumberWithLeadingZero('January').should.equal('01');
		});
		it('should return an string from the month name with a leading zero', () => {
			TimeUtils.TimeUtils.monthNameAsNumberWithLeadingZero('Jan').should.equal('01');
		});
		it('should return an string from the month name', () => {
			TimeUtils.TimeUtils.monthNameAsNumberWithLeadingZero('December').should.equal('12');
		});
		it('should return an string from the month name', () => {
			TimeUtils.TimeUtils.monthNameAsNumberWithLeadingZero('Dec').should.equal('12');
		});

		it('should return an human formatted date strings', () => {
			var momentDate = moment('31-01-2020', 'DD-MM-2020');
			TimeUtils.TimeUtils.momentAsHumanDate(momentDate).should.equal('Friday, January 31 2020');
		});
		it('should return an human formatted date strings', () => {
			var momentDate = moment('31-JAN-2020', 'DD-MMM-2020');
			TimeUtils.TimeUtils.momentAsHumanDate(momentDate).should.equal('Friday, January 31 2020');
		});

		it('should return a date string', () => {
			var momentDate = moment('31-JAN-2020', 'DD-MMM-2020');
			TimeUtils.TimeUtils.momentDateAsString(momentDate).should.equal('Friday, January 31 2020');
		});

		it('should return a time string', () => {
			var momentDate = moment('31-JAN-2020 10:00', 'DD-MMM-2020 HH:mm');
			TimeUtils.TimeUtils.momentTimeAsString(momentDate).should.equal('10:00');
		});
	});

	describe('Cleans a time string', () => {
		it('should return a formatted time string', () => {
			TimeUtils.TimeUtils.timeCheck('10:00').should.equal('10:00');
		});
		it('should return a formatted time string', () => {
			TimeUtils.TimeUtils.timeCheck('9:00').should.equal('09:00');
		});
		it('should return a formatted time string', () => {
			TimeUtils.TimeUtils.timeCheck('9:1').should.equal('09:01');
		});
		it('should return a formatted time string', () => {
			TimeUtils.TimeUtils.timeCheck('09:1').should.equal('09:01');
		});
	});
});